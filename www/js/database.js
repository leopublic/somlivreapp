var dbo = {
    // Propriedades
    db: null,
    ret: null,
    dados_sincronizacao: null,
    sql: null,
    id_pedido: null,
    nome_cliente: null,
    email: null,
    password: null,
    url: null,
    urlSignus: null,
    vendedor_id: null,
    unidade_negocio_id: null,
    id_genero: null,
    clientes: null,
    condpag: null,
    generos: null,
    produtos_classe: null,
    origem: null,
    // Metodos
    sqlOk: function () {
        console.log('Comando executado com sucesso!');
    },
    sqlError: function (error) {
        var self = this;
        app.hideLoading();
        alert('Erro sqllite: ' + self.sql + ' ' + error.code + error.message);
        console.log('Erro: ' + error.message);
    },
    ignore: function () {
    },
    // Recria o banco de dados
    ressetaBanco: function (processaOk) {
        var self = this;
        this.db.transaction(function (tx) {
            tx.executeSql("DROP TABLE IF EXISTS produto;", [], this.ignore, this.sqlError);
            tx.executeSql("DROP TABLE IF EXISTS musica;", [], this.ignore, this.sqlError);
            tx.executeSql("DROP TABLE IF EXISTS anexo;", [], this.ignore, this.sqlError);
            tx.executeSql("DROP TABLE IF EXISTS evento;", [], this.ignore, this.sqlError);
            tx.executeSql("DROP TABLE IF EXISTS config;", [], this.ignore, this.sqlError);
            tx.executeSql("DROP TABLE IF EXISTS pedido;", [], this.ignore, this.sqlError);
            tx.executeSql("DROP TABLE IF EXISTS itempedido;", [], this.ignore, this.sqlError);
            tx.executeSql("DROP TABLE IF EXISTS genero;", [], this.ignore, this.sqlError);
            tx.executeSql("DROP TABLE IF EXISTS produto_genero;", [], this.ignore, this.sqlError);
            tx.executeSql("DROP TABLE IF EXISTS cliente;", [], this.ignore, this.sqlError);
            tx.executeSql("DROP TABLE IF EXISTS produto_familia;", [], this.ignore, this.sqlError);
            tx.executeSql("DROP TABLE IF EXISTS condicao_pagamento;", [], this.ignore, this.sqlError);
            self.inicializaSchema(processaOk);
        }, self.sqlError, self.ignore);
    },
    // Cria as tabelas
    inicializaSchema: function (processaOk) {
        var self = this;
        this.db.transaction(function (tx) {
            var sql = "CREATE TABLE IF NOT EXISTS produto (" +
                    "id_produto INTEGER PRIMARY KEY ," +
                    "prod_cod INTEGER NULL," +
                    "catalogo VARCHAR(10) NOT NULL," +
                    "codigo_barras VARCHAR(20) NULL," +
                    "titulo VARCHAR(500) NOT NULL," +
                    "artista VARCHAR(500) NOT NULL," +
                    "preco DECIMAL(5,2) DEFAULT NULL," +
                    "imagem_destaque VARCHAR(500) DEFAULT NULL," +
                    "imagem_listagem VARCHAR(500) DEFAULT NULL," +
                    "icms_isento TINYINT DEFAULT NULL," +
                    "coletanea TINYINT DEFAULT NULL," +
                    "mais_vendido TINYINT DEFAULT NULL," +
                    "dt_importacao DATETIME DEFAULT NULL," +
                    "infantil TINYINT DEFAULT NULL," +
                    "lancamento TINYINT DEFAULT NULL," +
                    "texto_cheio varchar(2000) NULL ," +
                    "texto_reduzido varchar(2000) NULL ," +
                    "id_classe INTEGER NULL," +
                    "created_at DATETIME NULL," +
                    "updated_at DATETIME NULL," +
                    "baixado_em DATETIME NULL); ";
            tx.executeSql(sql, [], self.ignore, self.sqlError);

            var sql = "CREATE TABLE if not exists musica (" +
                    "id_musica INTEGER PRIMARY KEY ," +
                    "id_produto INTEGER NULL," +
                    "nome VARCHAR(500) NULL," +
                    "autor VARCHAR(500) NULL," +
                    "artista VARCHAR(500) NULL," +
                    "duracao VARCHAR(20) NULL," +
                    "ordem INTEGER NULL," +
                    "nome_original VARCHAR(1500) NULL," +
                    "created_at DATETIME NULL," +
                    "updated_at DATETIME NULL," +
                    "extensao VARCHAR(20) NULL);";
            tx.executeSql(sql, [], self.ignore, self.sqlError);

            var sql = "CREATE TABLE if not exists anexo (" +
                    "id_anexo INTEGER NOT NULL PRIMARY KEY ," +
                    "id_produto INTEGER NOT NULL," +
                    "nome VARCHAR(1500) NULL," +
                    "nome_original VARCHAR(1500) NULL," +
                    "created_at DATETIME NULL," +
                    "updated_at DATETIME NULL," +
                    "id_tipo INTEGER NULL," +
                    "extensao VARCHAR(20)  NULL);";
            tx.executeSql(sql, [], self.ignore, self.sqlError);

            var sql = "CREATE TABLE if not exists evento (" +
                    "id_evento INTEGER  NOT NULL PRIMARY KEY ," +
                    "id_produto INTEGER NOT NULL," +
                    "dt_agendada DATE NULL," +
                    "titulo VARCHAR(500) NULL," +
                    "descricao VARCHAR(2000) NULL," +
                    "created_at DATETIME NULL," +
                    "updated_at DATETIME NULL) ;";
            tx.executeSql(sql, [], self.ignore, self.sqlError);

            var sql = "CREATE TABLE if not exists genero (" +
                    "id_genero INTEGER  NOT NULL," +
                    "nome VARCHAR(500) NULL," +
                    "created_at DATETIME NULL," +
                    "updated_at DATETIME NULL) ;";
            tx.executeSql(sql, [], self.ignore, self.sqlError);

            var sql = "CREATE TABLE if not exists produto_genero (" +
                    "id_produto INTEGER NOT NULL," +
                    "id_genero INTEGER NOT NULL, "+
                    " PRIMARY KEY (id_produto, id_genero) ) ;";
            tx.executeSql(sql, [], self.ignore, self.sqlError);

            var sql = "CREATE TABLE if not exists config (" +
                    "chave VARCHAR(10) PRIMARY KEY," +
                    "url VARCHAR(500) NULL, " +
                    "email VARCHAR(500) NULL, " +
                    "password VARCHAR(20) NULL, " +
                    "vendedor_id INTEGER NULL, " +
                    "unidade_negocio_id INTEGER NULL, " +
                    "urlSignus VARCHAR(500) NULL, " +
                    "dt_ultima_sincronizacao datetime NULL); ";
            tx.executeSql(sql, [], self.ignore, self.sqlError);

            var sql = "CREATE TABLE if not exists pedido (" +
                    "id_pedido INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    "id_cliente integer NULL, " +
                    "id_condicao_pagamento integer NULL, " +
                    "numero integer NULL, " +
                    "cliente VARCHAR(500) NULL, " +
                    "atual TINYINT NULL, " +
                    "dt_pedido DATETIME NULL," +
                    "dt_envio DATETIME NULL," +
                    "statusSignus VARCHAR(500) NULL, " +
                    "msgSignus VARCHAR(500) NULL " +
                    "); ";
            self.sql = sql;
            tx.executeSql(sql, [], self.ignore, self.sqlError);

            var sql = "CREATE TABLE if not exists itempedido (" +
                    "id_itempedido INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    "id_pedido INTEGER NOT NULL ," +
                    "id_produto INTEGER NULL," +
                    "qtd INTEGER NULL," +
                    "desconto DECIMAL(5,2) NULL," +
                    "preco_bruto DECIMAL(5,2) DEFAULT NULL," +
                    "preco_liquido DECIMAL(5,2) DEFAULT NULL" +
                    "); ";
            self.sql = sql;
            tx.executeSql(sql, [], self.ignore, self.sqlError);

            var sql = "CREATE TABLE if not exists condicao_pagamento (" +
                    "id_condicao_pagamento INTEGER NOT NULL PRIMARY KEY," +
                    "descricao VARCHAR(50) NULL)";
            self.sql = sql;
            tx.executeSql(sql, [], self.ignore, self.sqlError);

            var sql = "CREATE TABLE if not exists cliente (" +
                    "id_cliente INTEGER NOT NULL PRIMARY KEY ," +
                    "cnpj VARCHAR(14) NULL," +
                    "razaosocial VARCHAR(50) NULL," +
                    "fantasia VARCHAR(20) NULL," +
                    "endereco VARCHAR(100) NULL," +
                    "bairro VARCHAR(50) NULL," +
                    "cep VARCHAR(8) NULL," +
                    "municipio VARCHAR(100) NULL," +
                    "estadosigla VARCHAR(02) NULL" +
                    ")";
            self.sql = sql;
            tx.executeSql(sql, [], self.ignore, self.sqlError);

            var sql = "CREATE TABLE if not exists familia (" +
                    "id_familia INTEGER NOT NULL PRIMARY KEY ," +
                    "nome VARCHAR(200) NULL  " +
                    ")";
            self.sql = sql;
            tx.executeSql(sql, [], self.ignore, self.sqlError);

            var sql = "CREATE TABLE if not exists produto_familia (" +
                    "id_familia INTEGER NOT NULL  ," +
                    "id_produto INTEGER NOT NULL,  " +
                    "PRIMARY KEY (id_familia, id_produto))";
            self.sql = sql;
            tx.executeSql(sql, [], self.ignore, self.sqlError);
        }, this.sqlError, processaOk);
    },
    // Sincroniza o banco local com o conteúdo do CMS
    atualizaConteudo: function () {
        $.getJSON("http://appvendas.somlivre.com/sinc/produtos")
                .done(function (data) {
                    var l = data.length;
                    $("#log").append('<br/>' + l + ' produtos recuperados');
                    console.log(l + ' produtos recuperados');
                    var sql = "INSERT OR REPLACE INTO produto " +
                            "(id_produto, catalogo, codigo_barras, titulo, artista, preco, imagem_destaque, imagem_listagem, icms_isento, coletanea, mais_vendido, infantil, lancamento, dt_importacao, texto_cheio, texto_reduzido, id_classe, created_at, updated_at, baixado_em) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, datetime('now'))";
                    var e;
                    this.db.transaction(function (tx) {
                        for (var i = 0; i < l; i++) {
                            e = data[i];
                            $("#log").append('<br/>Inserindo ' + e.id_produto + ' ' + e.catalogo);
                            tx.executeSql(sql, [e.id_produto, e.catalogo, e.codigo_barras, e.titulo, e.artista, e.preco, e.imagem_destaque, e.imagem_listagem, e.icms_isento, e.coletanea, e.mais_vendido, e.infantil, e.lancamento, e.dt_importacao, e.texto_cheio, e.texto_reduzido, e.id_classe, e.created_at, e.updated_at], this.sqlOk, this.sqlError);
                        }
                    }, this.sqlError, this.sqlOk);
                });
    },
    sincroniza: function () {
        var self = this;
        var sql;
        this.db.transaction(function (transaction) {
            sql = "delete from produto_genero; delete from genero;";
            dbo.sql = sql;
            transaction.executeSql(sql, [], function (tx, rs) {
                sql = "delete from genero;";
                dbo.sql = sql;
                transaction.executeSql(sql, [], function (tx, rs) {
                    dbo.executaSincroniza();
                }, self.sqlError);
            }, self.sqlError);
        }, self.sqlError, self.ignore);

    },
    executaSincroniza: function () {
        //var urlbase = config.url_atualizacao;
        var self = this;
        var url = dbo.url + "?email=" + dbo.email + '&password=' + dbo.password;
    
        app.logaSincronizacao('Recuperando atualização do banco de dados ' + url + '....</br>');
        $.ajax({
            url: url,
            type: "GET",
            dataType: "json"
        })
        .done(function (data) {
            dbo.dados_sincronizacao = data;
            app.logaSincronizacao(' Atualização recebida com sucesso! Iniciando processamento da atualização<br/>');
            var produtos = data.produtos;
            var musicas = data.musicas;
            var anexos = data.anexos;
            var eventos = data.eventos;
            var generos = data.generos;
            var produto_genero = data.produto_genero;
            var condpag = data.condpag;
            var clientes = data.clientes;
            var familias = data.familias;
            var produtos_familias = data.produtos_familias;
            var config = data.configuracao;
            var l = produtos.length;
            var lx = l;
            var sql;
            app.logaSincronizacao(l + ' produtos recuperados<br/>');
            dbo.db.transaction(function (tx) {
                controllerSincronizacao.carregaConfiguracao(tx, config);
                
                if (l > 0) {
                    var sql = "INSERT OR REPLACE INTO produto " +
                            "(id_produto, prod_cod, catalogo, titulo, artista, preco"+
                            ", imagem_destaque, imagem_listagem, icms_isento, coletanea, mais_vendido"+
                            ", infantil, lancamento, dt_importacao, texto_cheio, texto_reduzido"+
                            ", created_at, updated_at, baixado_em, id_classe, codigo_barras) " +
                            "VALUES "+
                            " (?, ?, ?, ?, ?, ?"+
                            ", ?, ?, ?, ?, ?"+
                            ", ?, ?, ?, ?, ?"+
                            ", ?, ?, datetime('now'), ?, ?)";
                    var e;
                    for (var i = 0; i < l; i++) {
                        e = produtos[i];
                        app.logaSincronizacao('Atualizando catálogo ' + e.catalogo + ' - ' + e.titulo + ' (id ' + e.id_produto + ')<br/>');
                        tx.executeSql(sql, [e.id_produto, e.prod_cod, e.catalogo, e.titulo, e.artista, e.preco, e.imagem_destaque, e.imagem_listagem, e.icms_isento, e.coletanea, e.mais_vendido, e.infantil, e.lancamento, e.dt_importacao, e.texto_cheio, e.texto_reduzido, e.created_at, e.updated_at, e.id_classe, e.codigo_barras], dbo.ignore, dbo.sqlError);
                    }
                } else {
                    app.logaSincronizacao('Nenhum produto importado<br/>');
                }
                app.logaSincronizacao('Encerrada sincronização de produtos<br/>');
                // Carrega musicas
                l = musicas.length;
                lx = l;
                app.logaSincronizacao(lx + ' músicas recuperadas<br/>');
                if (l > 0) {
                    var sql = "INSERT OR REPLACE INTO musica " +
                            "(id_musica, id_produto, nome, autor, artista, duracao, ordem, nome_original, extensao, created_at, updated_at) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    var e;
                    for (var i = 0; i < l; i++) {
                        e = musicas[i];
                        app.logaSincronizacao('Atualizando música ' + e.nome + ' - ' + e.autor + ' (id ' + e.id_musica + ')<br/>');
                        tx.executeSql(sql, [e.id_musica, e.id_produto, e.nome, e.autor, e.artista, e.duracao, e.ordem, e.nome_original, e.extensao, e.created_at, e.updated_at]
                                , dbo.ignore
                                , dbo.sqlError);
                    }
                } else {
                    app.logaSincronizacao('Nenhuma música importada<br/>');
                }
                app.logaSincronizacao('Encerrada sincronização de músicas<br/>');
                // Carrega eventos
                l = eventos.length;
                lx = l;
                app.logaSincronizacao(lx + ' eventos recuperados<br/>');
                if (l > 0) {
                    var sql = "INSERT OR REPLACE INTO evento " +
                            "(id_evento, id_produto, dt_agendada, titulo, descricao, created_at, updated_at) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?)";
                    var e;
                    for (var i = 0; i < l; i++) {
                        e = eventos[i];
                        app.logaSincronizacao('Atualizando evento ' + e.dt_agendada + ' - ' + e.titulo + ' (id ' + e.id_evento + ')<br/>');
                        tx.executeSql(sql, [e.id_evento, e.id_produto, e.dt_agendada, e.titulo, e.descricao, e.created_at, e.updated_at], dbo.ignore, dbo.sqlError);
                    }
                } else {
                    app.logaSincronizacao('Nenhum evento importado<br/>');
                }
                app.logaSincronizacao('Encerrada sincronização de eventos<br/>');
                // Carrega anexos
                l = anexos.length;
                lx = l;
                app.logaSincronizacao(lx + ' anexos recuperados<br/>');
                if (l > 0) {
                    var sql = "INSERT OR REPLACE INTO anexo " +
                            "(id_anexo, id_produto, nome, nome_original, extensao, created_at, updated_at) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?)";
                    var e;
                    for (var i = 0; i < l; i++) {
                        e = anexos[i];
                        app.logaSincronizacao('Atualizando anexo ' + e.nome + ' - ' + e.extensao + ' (id ' + e.id_anexo + ')<br/>');
                        tx.executeSql(sql, [e.id_anexo, e.id_produto, e.nome, e.nome_original, e.extensao, e.created_at, e.updated_at], dbo.ignore, dbo.sqlError);
                    }
                } else {
                    app.logaSincronizacao('Nenhum anexo importado<br/>');
                }
                app.logaSincronizacao('Encerrada sincronização de generos<br/>');
                // Carrega generos
                l = generos.length;
                lx = l;
                app.logaSincronizacao(lx + ' generos recuperados<br/>');
                if (l > 0) {
                    var sql = "INSERT OR REPLACE INTO genero " +
                            "(id_genero, nome, created_at, updated_at) " +
                            "VALUES (?, ?, ?, ?)";
                    var e;
                    for (var i = 0; i < l; i++) {
                        e = generos[i];
                        app.logaSincronizacao('Atualizando genero ' + e.nome + ' (id ' + e.id_genero + ')<br/>');
                        tx.executeSql(sql, [e.id_genero, e.nome, e.created_at, e.updated_at], dbo.ignore, dbo.sqlError);
                    }
                } else {
                    app.logaSincronizacao('Nenhum genro importado<br/>');
                }
                app.logaSincronizacao('Encerrada sincronização de produto_genero<br/>');

                // Carrega produto generos
                l = produto_genero.length;
                lx = l;                
                app.logaSincronizacao(lx + ' produto-gêneros recuperados<br/>');
                if (l > 0) {
                    var sql = "INSERT OR REPLACE INTO produto_genero " +
                            "(id_produto, id_genero) " +
                            "VALUES (?, ?)";
                    var e;
                    for (var i = 0; i < l; i++) {
                        e = produto_genero[i];
                        app.logaSincronizacao('Atualizando produto-gênero ' + e.id_produto + ': ' + e.id_genero + '<br/>');
                        tx.executeSql(sql, [e.id_produto, e.id_genero], dbo.ignore, dbo.sqlError);
                    }
                } else {
                    app.logaSincronizacao('Nenhum produto-genero importado<br/>');
                }

                // Carrega familias
                l = familias.length;
                lx = l;                
                app.logaSincronizacao(lx + ' familias recuperadas<br/>');
                if (l > 0) {
                    var sql = "INSERT OR REPLACE INTO familia " +
                            "(id_familia, nome) " +
                            "VALUES (?, ?)";
                    var e;
                    for (var i = 0; i < l; i++) {
                        e = familias[i];
                        app.logaSincronizacao('Atualizando familias ' + e.id_familia+ ': ' + e.nome+ '<br/>');
                        tx.executeSql(sql, [e.id_familia, e.nome], dbo.ignore, dbo.sqlError);
                    }
                } else {
                    app.logaSincronizacao('Nenhuma familia importado<br/>');
                }
                app.logaSincronizacao('Encerrada sincronização de familias<br/>');

                // Carrega produtofamilias
                l = produtos_familias.length;
                lx = l;                
                app.logaSincronizacao(lx + ' relacionamentos de familia recuperados<br/>');
                if (l > 0) {
                    var sql = "INSERT OR REPLACE INTO produto_familia " +
                            "(id_familia, id_produto) " +
                            "VALUES (?, ?)";
                    var e;
                    for (var i = 0; i < l; i++) {
                        e = produtos_familias[i];
                        app.logaSincronizacao('Atualizando relacionamento familias ' + e.id_familia+ ': produto ' + e.id_produto+ '<br/>');
                        tx.executeSql(sql, [e.id_familia, e.id_produto], dbo.ignore, dbo.sqlError);
                    }
                } else {
                    app.logaSincronizacao('Nenhum produto-familia importado<br/>');
                }
                app.logaSincronizacao('Encerrada sincronização de produtos-familias<br/>');

            }, dbo.sqlErro, dbo.sincronizouOk);
        })
        .error(function (jqXHR, erro, erroHTTP) {
            app.logaSincronizacao('Erro na sincronização:' + erro + '<br/>' +  erroHTTP + '<br/>' + jqXHR.responseText + '<br/>');
        })
        .fail(function (jqXHR, erro) {
            app.logaSincronizacao('Erro na sincronização:' + erro + '<br/>' + jqXHR.responseText + '<br/>');
        });

    },
    sincronizouOk: function () {
        app.logaSincronizacao('Encerrada sincronização do banco de dados<br/>');
        baixaArquivos();
    },
    inicializaApp: function () {
        dbo.carregaConfiguracao();
    },
    carregaConfiguracao: function () {
        var self = this;
        var row = '';
        this.db.transaction(function (transaction) {
            var sql;
            sql = "select * from config where chave='principal' ";
            transaction.executeSql(sql, [], function (tx, rs) {
                if (rs != null && rs.rows != null && rs.rows.length > 0) {
                    row = rs.rows.item(0);
                    self.email = row.email;
                    self.password = row.password;
                    self.url = row.url;
                    self.vendedor_id = row.vendedor_id;
                    self.unidade_negocio_id = row.unidade_negocio_id;
                    self.urlSignus = row.urlSignus;
                } else {
                    self.email = 'representante@somlivre.com.br';
                    self.password = 'somlivre';
                    self.url = 'http://appvendas.somlivre.com/sinc/';
                    self.urlSignus = 'http://189.44.180.59/SIGNUSERPIntegracaoCMSM2/WSIntegracaoCMS.svc';
                    sql = "insert into config(chave, email, password, url, urlSignus) values('principal', 'representante@somlivre.com.br', 'somlivre', 'http://appvendas.somlivre.com/sinc/', 'http://189.44.180.59/SIGNUSERPIntegracaoCMSM2/WSIntegracaoCMS.svc') ";
                    transaction.executeSql(sql, [], function (tx, rs) {
                    }, self.sqlError);
                }
                self.recarregaClientes();
                self.recarregaCondpag();
                self.recarregaGeneros();
            }, self.sqlError);
        }, self.sqlError, dbo.abrePedidoAtual);
    },
    abrePedidoAtual: function () {
        var self = this;
        var sql;
        this.db.transaction(function (transaction) {
            sql = "select id_pedido, pedido.id_cliente, fantasia, razaosocial";
            sql+= "  from pedido ";
            sql+="   join cliente on cliente.id_cliente = pedido.id_cliente";
            sql+= " where atual = 1 and dt_envio is null order by id_pedido desc";
            transaction.executeSql(sql, [], function (tx, rs) {
                if (rs != null && rs.rows != null && rs.rows.length > 0) {
                    var row = rs.rows.item(0);
                    dbo.id_pedido = row.id_pedido;
                    dbo.alteraCabecalho(row.razaosocial, row.id_cliente, '');
                } else {
                    dbo.id_pedido = '';
                    dbo.alteraCabecalho('', '', '');
                }
                dbo.atualizaCabecalhosProdutos();
            }, self.sqlError);
        }, self.sqlError, dbo.abreLancamentos);
    },
    recarregaClientes: function () {
        var self = this;
        var sql;
        this.db.transaction(function (transaction) {
            sql = "select * from cliente order by fantasia ";
            transaction.executeSql(sql, [], function (tx, rs) {
                var clientes = [];
                for (var i = 0; i < rs.rows.length; i++) {
                    row = rs.rows.item(i);
                    clientes[clientes.length] = row;
                }
                dbo.clientes = clientes;
            }, self.sqlError);
        }, self.sqlError, self.ignore());

    },
    recarregaCondpag: function () {
        var self = this;
        var sql;
        this.db.transaction(function (transaction) {
            sql = "select * from condicao_pagamento order by descricao ";
            transaction.executeSql(sql, [], function (tx, rs) {
                var condpag = [];
                for (var i = 0; i < rs.rows.length; i++) {
                    row = rs.rows.item(i);
                    condpag[condpag.length] = row;
                }
                dbo.condpag = condpag;
            }, self.sqlError);
        }, self.sqlError, self.ignore());
    },
    recarregaGeneros: function () {
        var self = this;
        var sql;
        this.db.transaction(function (transaction) {
            sql = "select * from genero order by nome";
            transaction.executeSql(sql, [], function (tx, rs) {
                var generos = [];
                for (var i = 0; i < rs.rows.length; i++) {
                    row = rs.rows.item(i);
                    generos[generos.length] = row;
                }
                dbo.generos = generos;
            }, self.sqlError);
        }, self.sqlError, self.ignore());
    },

    abreLancamentos: function () {
        var self = this;
        var dados = [];
        this.db.transaction(function (transaction) {
            var sql;
            sql = "select * from produto where id_classe=1 order by catalogo";
            transaction.executeSql(sql, [], function (tx, rs) {
                if (rs != null && rs.rows != null) {
                    var linhas = [];
                    for (var i = 0; i < rs.rows.length; i++) {
                        var row = rs.rows.item(i);
                        linhas[linhas.length] = row;
                    }
                    dados['produtos'] = linhas;
                    dados['titulo'] = app.getTitulo();
                    $('#conteudo_coverflow').remove();
                    $('#panel-home').append('<div role="main" class="ui-content-b" id="conteudo_coverflow"></div>');
                    $('#conteudo_coverflow').html(app.coverflow(linhas));
                    $('#coverflow').coverFlow();
                }
            }, self.sqlError);
        }, self.sqlError, dbo.abreCatalogo);
    },

    recuperaProdutoClasse: function () {
        var self = this;
        this.db.transaction(function (transaction) {
            var sql;
            sql = "select * from produto where id_classe=1 order by catalogo";
            transaction.executeSql(sql, [], function (tx, rs) {
                if (rs != null && rs.rows != null) {
                    var linhas = [];
                    for (var i = 0; i < rs.rows.length; i++) {
                        var row = rs.rows.item(i);
                        linhas[linhas.length] = row;
                    }
                    $('.coverflow').remove();
                    $('#conteudo_coverflow').remove();
                    $('#panel-home').append('<div role="main" class="ui-content-b" id="conteudo_coverflow"></div>');
                    $('#conteudo_coverflow').html(app.coverflow(linhas));
                    $('#coverflow').coverFlow();
                }
            }, self.sqlError);
        }, self.sqlError, dbo.abreCatalogo);
    },
    
    abreCatalogo: function () {
        var self = this;
        dbo.db.transaction(function (transaction) {
            var sql;
            sql = "select * from produto order by id_produto desc limit 300,300";
            transaction.executeSql(sql, [], function (tx, rs) {
                var linhas = [];
                var x = rs.rows.length;
                if (rs != null && rs.rows != null) {
                    for (var i = 0; i < rs.rows.length; i++) {
                        var row = rs.rows.item(i);
                        row.preco = app.formatNumber(row.preco);
                        linhas[linhas.length] = row;
                        x = row.id_produto;
                    }
                }
                $('#conteudo_catalogo').html(app.catalogo(linhas));
                $('#panel-catalogo').children('#tabCata').removeClass('ui-btn-active');
                $('#panel-catalogo').page().enhanceWithin();
            }, dbo.sqlError);
        }, dbo.sqlError, dbo.abreGeneros);

    },
    abreGeneros: function () {
        var self = this;
        var sql;
        var dados = [];
        dbo.db.transaction(function (transaction) {
                dados['generos'] = dbo.generos;
                sql = "select produto.* from produto order by titulo limit 300,300";
                transaction.executeSql(sql, [], function (tx, rs) {
                    var linhas = [];
                    if (rs != null && rs.rows != null) {
                        for (var i = 0; i < rs.rows.length; i++) {
                            var row = rs.rows.item(i);
                            row.preco = app.formatNumber(row.preco);
                            linhas[linhas.length] = row;
                        }
                    }
                    dados['produtos'] = linhas;
                    $('#conteudo_genero').html(app.genero(dados));
                    $('#panel-genero').page().enhanceWithin();
                    app.hideLoading();
                }, self.sqlError);
        }, self.sqlError, self.ignore);
    },

    debug_erro: function (error) {
        alert('Erro ' + error);
        $('#debug_resultado').append('<br>Erro ' + JSON.stringify(error));
    },
    debug_ok: function () {
        $('#debug_resultado').append('<br>ok');

    },
    debug: function (sql) {
        var self = this;
        this.db.transaction(function (transaction) {
            transaction.executeSql(sql, [], function (tx, rs) {
                if (rs == null) {
                    $('#debug_resultado').append('<br>resultado é nulo');
                    $('#debug_resultado').append('rs=' + JSON.stringify(rs));
                } else {
                    if (rs.rows == null) {
                        $('#debug_resultado').append('<br>resultado é nulo');
                    } else {
                        var x = rs.rows.length;
                        var y = rs.rows;
                        $('#debug_resultado').append('<br>recuperadas ' + x + ' linhas');
                        for (var i = 0; i < rs.rows.length; i++) {
                            var row = rs.rows.item(i);
                            $('#debug_resultado').append(JSON.stringify(row));
                        }
                    }
                }
            }, self.debug_erro);
        }, self.debug_erro, self.debug_ok);
    },
    query: function (sql, processa) {
        var self = this;
        this.db.transaction(function (transaction) {
            transaction.executeSql(sql, [], processa, self.debug_error);
        }, self.debug_error, self.debug_ok);
    },
    // Abre a conexao com o banco de dados, criando o mesmo caso não exista
    abra: function () {
        this.db = window.sqlitePlugin.openDatabase({name: "appvendas.db"});
        this.inicializaSchema(this.ignore);
    },
    //
    // Cria um novo pedido
    criarPedido: function (id_cliente, fantasia) {
        var self = this;
        var id = null;
        var sql;
        this.nome_cliente = fantasia;
        this.db.transaction(function (transaction) {
            sql = "update pedido set atual = 0";
            transaction.executeSql(sql, [], function (tx, rs) {
                sql = "insert into pedido (id_cliente, dt_pedido, atual) values (?, datetime('now','localtime'), 1) ";
                transaction.executeSql(sql, [id_cliente], function (tx, rs) {
                    // Deu tudo certo. Redireciona para home
                    self.id_pedido = rs.insertId;
                    $('.titulogeral').html('Catálogo Virtual - Pedido para "' + self.nome_cliente + '"');
                    $(":mobile-pagecontainer").pagecontainer("change", "#panel-home");
                }, self.sqlError);
            }, self.sqlError);
        }, self.sqlError, self.ignore);
    },
    //
    // Editar pedido
    editarPedido: function (id_cliente, data_solicitacao, mm) {
        var self = this;
        var id = null;
        var sql;
        this.nome_cliente = fantasia;
        this.db.transaction(function (transaction) {
            sql = "update pedido set atual = 0";
            transaction.executeSql(sql, [], function (tx, rs) {
                sql = "insert into pedido (id_cliente, dt_pedido, atual) values (?, datetime('now','localtime'), 1) ";
                transaction.executeSql(sql, [id_cliente], function (tx, rs) {
                    // Deu tudo certo. Redireciona para home
                    self.id_pedido = rs.insertId;
                    $('.titulogeral').html('Catálogo Virtual - Pedido para "' + self.nome_cliente + '"');
                    $(":mobile-pagecontainer").pagecontainer("change", "#panel-home");
                }, self.sqlError);
            }, self.sqlError);
        }, self.sqlError, self.ignore);
    },
    inseriuComSucesso: function(){
        $(":mobile-pagecontainer").pagecontainer("change", dbo.origem);
    },

    listarCondpag: function (msg) {
        var self = this;
        this.db.transaction(function (transaction) {
            var sql;
            sql = "select * from condicao_pagamento order by descricao ";
            transaction.executeSql(sql, [], function (tx, rs) {
                var condpag = [];
                for (var i = 0; i < rs.rows.length; i++) {
                    var row = rs.rows.item(i);
                    condpag[condpag.length] = row;
                }
                $('#condpag').html(app.condpag(condpag));
                $(":mobile-pagecontainer").pagecontainer("change", "#panel-condpag");
                if (msg != '' && typeof msg != 'undefined') {
                    $("#msgPopup").html(msg);
                    $("#popupMsg").popup("open");
                }
            }, self.sqlError);
        }, self.sqlError, self.ignore);
    },
    abrirPedido: function (msg) {
        var self = this;
        sql = "select id_cliente, razaosocial from cliente order by razaosocial";
        dbo.sql = sql;
        var pedido = [];
        this.db.transaction(function (tx) {
            tx.executeSql(sql, [], function (tx, rs) {
                if (rs != null && rs.rows != null && rs.rows.length > 0) {
                    var clientes = [];
                    for (var i = 0; i < rs.rows.length; i++) {
                        var row = rs.rows.item(i);
                        clientes[clientes.length] = row;
                    }
                    pedido['clientes'] = clientes;
                } else {
                    pedido['clientes'] = [];
                }
                $('#abrirpedido-content').html(app.abrirpedido(pedido));
                $('#panel-abrirpedido').page().enhanceWithin();
                $(":mobile-pagecontainer").pagecontainer("change", "#panel-abrirpedido");
                if (msg != '' && typeof msg != 'undefined') {
                    $("#msgPopup").html(msg);
                    $("#popupMsg").popup("open");
                }
            }, self.sqlError);
        }, self.sqlError, self.ignore);

    },
    numeroFormatado: function (numero, casas) {
        numero = Number(numero).toFixed(casas);
        var parts = (numero + "").split("."),
                main = parts[0],
                len = main.length,
                output = "",
                i = len - 1;

        while (i >= 0) {
            output = main.charAt(i) + output;
            if ((len - i) % 3 === 0 && i > 0) {
                output = "." + output;
            }
            --i;
        }
        // put decimal part back
        if (parts.length > 1) {
            output += "," + parts[1];
        }
        return output;
    },
    salvaConfiguracao: function (email, password, url, vendedor_id, unidade_negocio_id, urlSignus) {
        this.db.transaction(function (transaction) {
            var sql;
            sql = "update config set email = ?, password = ?, url = ?, vendedor_id = ?, unidade_negocio_id = ?, urlSignus = ? where chave = 'principal'";
            transaction.executeSql(sql, [email, password, url, vendedor_id, unidade_negocio_id, urlSignus], function (tx, rs) {
                dbo.email = email;
                dbo.password = password;
                dbo.url = url;
                dbo.vendedor_id = vendedor_id;
                dbo.unidade_negocio_id = unidade_negocio_id;
                dbo.urlSignus = urlSignus;
                $("#msgConfiguracao #msgPopup").html("Configuração atualizada com sucesso!");
                $("#msgConfiguracao").popup("open");
            }, self.sqlError);
        }, self.sqlError, self.ignore);
    },
    abreConfiguracao: function () {
        var self = this;
        $('#email').val(self.email);
        $('#password').val(self.password);
        $('#url').val(self.url);
        $('#vendedor_id').val(self.vendedor_id);
        $('#unidade_negocio_id').val(self.unidade_negocio_id);
        $('#urlSignus').val(self.urlSignus);
        $(":mobile-pagecontainer").pagecontainer("change", "#panel-configuracao");
    },
    
    listaPorGenero: function (id_genero) {
        var self = this;
        var sql;
        var dados = [];
        dbo.db.transaction(function (transaction) {
            var generos = dbo.generos;
            for (var i = 0; i < generos.length; i++) {
                if (generos[i].id_genero == id_genero) {
                    generos[i].selected = 'selected';
                } else {
                    generos[i].selected = null;
                }
            }
            dados['generos'] = generos;
            var parametros = [];
            if (id_genero != '' && typeof (id_genero) != 'undefined') {
                dbo.id_genero = id_genero;
                sql = "select produto.* from produto, produto_genero where produto.id_produto = produto_genero.id_produto and produto_genero.id_genero = ? order by titulo limit 100";
                parametros[parametros.length] = id_genero;
            } else {
                dbo.id_genero = null;
                sql = "select produto.* from produto order by id_produto desc limit 100";
            }
            transaction.executeSql(sql, parametros, function (tx, rs) {
                var linhas = [];
                if (rs != null && rs.rows != null) {
                    for (var i = 0; i < rs.rows.length; i++) {
                        var row = rs.rows.item(i);
                        row.preco = app.formatNumber(row.preco);
                        if (row.id_classe == 1){
                            row.origem = '#panel-home';
                        } else {
                            row.origem = '#panel-produtos' + row.id_classe
                        }
                        linhas[linhas.length] = row;
                    }
                }
                dados['produtos'] = linhas;
                $('#conteudo_genero').html(app.genero(dados));
                $('#panel-genero').page().enhanceWithin();
                app.hideLoading();
            }, self.sqlError);
        }, self.sqlError, self.ignore);
    },
    strXML_consultas: function () {
        var ret = "<![CDATA[<?xml version=\"1.0\"?><Solicitacao><VendedorID>" + dbo.vendedor_id + "</VendedorID><UnidadeNegocioID>" + dbo.unidade_negocio_id + "</UnidadeNegocioID></Solicitacao>]]>";
        return ret;
    },
    cadastraProdutos: function () {
        var self = this;
        dbo.db.transaction(function (tx) {
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (40563,'0562-9         ','7891430056292','GALINHA PINTADINHA 2                              ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (41193,'0825-9         ','7891430082598','GALINHA PINTADINHA 3                              ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (41645,'2782-2         ','7891430278229','ZECA BALEIRO - CALMA AÍ, CORAÇÃO - AO VIVO - CD   ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (41646,'0951-9         ','7891430095192','ZECA BALEIRO - CALMA AÍ, CORAÇÃO - AO VIVO -  DVD ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (41707,'3264-2         ','7891430326425','VICTOR & LEO - VIVA POR MIM                       ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (41745,'2824-2         ','7891430282424','JORGE & MATEUS - LIVE IN LONDON - AT ROYAL ALBERT ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (41746,'0958-9         ','7891430095895','JORGE & MATEUS - LIVE IN LONDON - AT ROYAL ALBERT ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (41870,'3359-2         ','7891430335922','GUSTTAVO LIMA - DO OUTRO LADO DA MOEDA            ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (41911,'3398-2         ','7891430339821','TITÃS - NHEENGATU - CD                            ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (41921,'3455-2         ','7891430345525','ROSA DE SARON - CARTAS AO REMETENTE               ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (41929,'3433-2         ','7891430343323','LOVE HITS 2014                                    ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (41950,'0956-9         ','7891430095697','GALINHA PINTADINHA 4 - DVD                        ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (41961,'3393-2         ','7891430339326','CÉSAR MENOTTI & FABIANO - MEMÓRIAS ANOS 80 E 90 - ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (42036,'3684-2         ','7891430368425','MALTA - SUPERNOVA                                 ', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
            sql = "insert into produto(id_produto, catalogo, codigo_barras, titulo, artista) values (42039,'3664-2         ','7891430366421','PADRE ALESSANDRO CAMPOS - O QUE É QUE EU SOU SEM J', '');";
            tx.executeSql(sql, [], self.ignore, self.sqlError);
        }, self.sqlError, self.ignore);

    },
    
    atualizaCabecalhosProdutos: function(){
        $('#panel-produtos .titulogeral').html(dbo.nome_cliente);
        $('#panel-home .titulogeral').html(dbo.nome_cliente);
        $('#panel-genero .titulogeral').html(dbo.nome_cliente);
        $('#panel-produtos2 .titulogeral').html(dbo.nome_cliente);
        $('#panel-produtos3 .titulogeral').html(dbo.nome_cliente);
        $('#panel-produtos4 .titulogeral').html(dbo.nome_cliente);
    },
    
    alteraCabecalho: function(razaosocial, id_cliente, fantasia){
        if (id_cliente != ''){
            dbo.nome_cliente = 'Catálogo Virtual - Pedido para "'+razaosocial+' ('+id_cliente+')"';            
        } else {
            dbo.nome_cliente = 'Catálogo Virtual - (abra um novo pedido)';            
        }
    }

};
