var controllerProduto = {
    // Propriedades
    db: null,
    sql: null,
    cache: null,
    produtos_carregados: [],
    produtos_classe: [],
    resultado: null,
    ret: null,
    origem: null,
    // Metodos
    sqlOk: function () {
        console.log('Comando executado com sucesso!');
    },
    sqlError: function (error) {
        var self = this;
        app.hideLoading();
        alert('Erro sqllite: ' + self.sql + ' ' + error.code + error.message);
        console.log('Erro: ' + error.message);
    },
    ignore: function () {
    },
    // Abre a conexao com o banco de dados, criando o mesmo caso não exista
    abra: function () {
        this.db = window.sqlitePlugin.openDatabase({name: "appvendas.db"});
    },
    listar: function (msg) {
        var self = this;
        this.db.transaction(function (transaction) {
            var sql;
            sql = "select * from produto order by titulo limit 200";
            transaction.executeSql(sql, [], function (tx, rs) {
                var linhas = [];
                for (var i = 0; i < rs.rows.length; i++) {
                    var row = rs.rows.item(i);
                    linhas[linhas.length] = row;
                }
                $('#panel-cadastros').html(app.tmplProdutoListar(linhas));
                $('#panel-cadastros').page().enhanceWithin();
                $("#produtos-table").floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('#produtos-content');
                    }
                });
                $(":mobile-pagecontainer").pagecontainer("change", "#panel-cadastros", {"allowSamePageTransition": true});
                if (msg != '' && typeof msg != 'undefined') {
                    $("#msgPopup").html(msg);
                    $("#popupMsg").popup("open");
                }
            }, self.sqlError);
        }, self.sqlError, self.ignore);
    },
    carregar: function () {
        this.produtos_classe = null;
        this.produtos_classe = [];
        this.carregarPorClasse(1);
        this.carregarPorClasse(2);
        this.carregarPorClasse(3);
        this.carregarPorClasse(4);
    },
    carregarPorClasse: function (id_classe) {
        var self = this;
        this.db.transaction(function (transaction) {
            var sql;
            sql = "select * from produto where id_classe=? order by titulo ";
            transaction.executeSql(sql, [id_classe], function (tx, rs) {
                var linhas = [];
                var x = rs.rows.length;
                if (rs != null && rs.rows != null) {
                    for (var i = 0; i < rs.rows.length; i++) {
                        var row = rs.rows.item(i);
                        row.preco_fmt = dbo.numeroFormatado(row.preco, 2);
                        linhas[linhas.length] = row;
                        x = row.id_produto;
                    }
                }
                controllerProduto.produtos_classe[id_classe] = linhas;
            }, self.sqlError);
        }, self.sqlError, self.abreGeneros);
    },
    getMenu: function (ativo) {
        var menu = {
            '1': {texto: 'Lançamentos', id: '1', href: '#panel-home', onclick: "", ativo: ''}
            , '2': {texto: 'Mídia', id: '2', href: '', onclick: "controllerProduto.getListar('2','');", ativo: ''}
            , '3': {texto: 'Destaque', id: '3', href: '', onclick: "controllerProduto.getListar('3','');", ativo: ''}
            , '4': {texto: 'Catálogo', id: '4', href: '', onclick: "controllerProduto.getListar('4','');", ativo: ''}
        };
        if (ativo > 1) {
            menu[ativo].ativo = '1';
        }
        return menu;
    },
    getListar: function (id_classe, msg) {
        app.showLoading('carregando');
        if (this.produtos_carregados[id_classe]) {

        } else {
            this.produtos_carregados[id_classe] = true;
            var self = this;
            var dados = [];
            var origem = '#panel-produtos' + id_classe;
            dados['itensMenu'] = this.getMenu(id_classe);
            dados['produtos'] = this.produtos_classe[id_classe];
            dados['origem'] = origem;
            dados['titulo'] = dbo.nome_cliente;
            $('#panel-produtos' + id_classe).html(app.tmplProdutoListarPorClasse(dados));
            $('#panel-produtos' + id_classe).page().enhanceWithin();
            $(origem + " #msgProduto").popup({transition: "fade"});
            $(origem + " #msgProduto").on("popupafteropen", function (event, ui) {
                setTimeout(function () {
                    $(origem + " #msgProduto").popup("close");
                }, 1500);
            });
        }
        $(":mobile-pagecontainer").pagecontainer("change", "#panel-produtos" + id_classe);
        app.hideLoading();
    },
    distribuiClasses: function () {
        var self = this;
        var sql = "update produto set id_classe = (id_produto % 4) + 1;";
        this.db.transaction(function (transaction) {
            transaction.executeSql(sql, [], self.ignore, self.sqlError);
        }, self.sqlError, self.ignore);
    },
    show: function (id_produto, origem) {
        var self = this;
        var produto;
        controllerProduto.origem = origem;
        $("#popupAdicionaProduto").popup("destroy");
        this.db.transaction(function (transaction) {
            var sql;
            sql = "select id_produto, prod_cod, catalogo, titulo, artista, ifnull(preco,0) preco " +
                    ", imagem_destaque, imagem_listagem, icms_isento, coletanea, mais_vendido" +
                    ", infantil, lancamento, dt_importacao, texto_cheio, texto_reduzido" +
                    ", created_at, updated_at, baixado_em, id_classe, codigo_barras " +
                    " from produto where id_produto = ?";
            transaction.executeSql(sql, [id_produto], function (tx, rs) {
                var row = rs.rows.item(0);
                row.preco = dbo.numeroFormatado(row.preco, 2);
                if(row.id_classe == 1){
                    row.id_classe1 = true;
                }
                if(row.id_classe == 2){
                    row.id_classe2 = true;
                }
                if(row.id_classe == 3){
                    row.id_classe3 = true;
                }
                if(row.id_classe == 4){
                    row.id_classe4 = true;
                }
                
                produto = row;
                sql = "select produto.id_produto, produto.id_classe, titulo, artista "+
                        " from produto, produto_familia " +
                        " where produto.id_produto = produto_familia.id_produto " +
                        " and produto_familia.id_familia in (select id_familia from produto_familia where id_produto = ?)"+
                        " and produto.id_produto <> ?";

                tx.executeSql(sql, [id_produto, id_produto], function (tx, rs) {
                    var relacionados = [];
                    if (rs != null && rs.rows != null) {
                        for (var i = 0; i < rs.rows.length; i++) {
                            var row = rs.rows.item(i);
                            if (row.id_classe == 1){
                                row.origem = '#panel-home';
                            } else {
                                row.origem = '#panel-produtos' + row.id_classe
                            }
                            relacionados[relacionados.length] = row;
                        }
                    }
                    produto['relacionados'] = relacionados;

                    sql = "select nome from produto_genero, genero where genero.id_genero = produto_genero.id_genero and id_produto = ?";
                    tx.executeSql(sql, [id_produto], function (tx, rs) {
                        var generos = '';
                        var virgula = '';
                        if (rs != null && rs.rows != null) {
                            var linhas = [];
                            for (var i = 0; i < rs.rows.length; i++) {
                                var row = rs.rows.item(i);
                                generos = generos + virgula + row.nome;
                                virgula = ', ';
                            }
                        }
                        if (generos == '') {
                            generos = '(nenhum)';
                        }
                        produto['generos'] = generos;
                        produto['titulogeral'] = dbo.nome_cliente;
                        $('#panel-produtodetalhe').html(app.catalogo_show(produto));
                        $('#panel-produtodetalhe').page().enhanceWithin();
                        $("#popupAdicionaProduto").on("popupafterclose", function () {
                            controllerProduto.notificaResultadoInclusaoItem();
                        });

                        $(":mobile-pagecontainer").pagecontainer("change", "#panel-produtodetalhe");
                    }, dbo.sqlError);
                }, dbo.sqlError);
            }, dbo.sqlError);
        }, dbo.sqlError, dbo.ignore);
    },
    notificaResultadoInclusaoItem: function () {
        var origem = controllerProduto.origem;
        if (controllerProduto.ret == 1) {
            $(":mobile-pagecontainer").pagecontainer("change", origem);
            $(origem + " #msgPopup").html(controllerProduto.resultado);
            $(origem + " #msgProduto").popup("open");
        }
    },
    showLancamento: function (id_produto) {
        var self = this;
        var produto;
        app.showLoading('Carregando');
        controllerProduto.origem = '#panel-home';
        this.db.transaction(function (transaction) {
            var sql;
            sql = "select * from produto where id_produto = ?";
            transaction.executeSql(sql, [id_produto], function (tx, rs) {
                var row = rs.rows.item(0);
                row.preco = app.formatNumber(row.preco);
                produto = row;
                $(":mobile-pagecontainer").pagecontainer("change", "#panel-home");
                sql = "select * from musica where id_produto = ?";
                transaction.executeSql(sql, [id_produto], function (tx, rs) {
                    if (rs != null && rs.rows != null) {
                        var linhas = [];
                        for (var i = 0; i < rs.rows.length; i++) {
                            var row = rs.rows.item(i);
                            linhas[linhas.length] = row;
                        }
                        produto['musicas'] = linhas;
                    }
                    sql = "select * from evento where id_produto = ?";
                    transaction.executeSql(sql, [id_produto], function (tx, rs) {
                        if (rs != null && rs.rows != null) {
                            var linhas = [];
                            for (var i = 0; i < rs.rows.length; i++) {
                                var row = rs.rows.item(i);
                                if (row.dt_agendada != '') {
                                    row.dt_agendada = row.dt_agendada.substr(8, 2) + '/' + row.dt_agendada.substr(5, 2) + '/' + row.dt_agendada.substr(0, 4);
                                }
                                linhas[linhas.length] = row;
                            }
                            produto['eventos'] = linhas;
                        }
                        sql = "select * from anexo where id_produto = ?";
                        transaction.executeSql(sql, [id_produto], function (tx, rs) {
                            if (rs != null && rs.rows != null) {
                                var linhas = [];
                                for (var i = 0; i < rs.rows.length; i++) {
                                    var row = rs.rows.item(i);
                                    linhas[linhas.length] = row;
                                }
                                produto['anexos'] = linhas;
                            }
                            sql = "select produto.id_produto, produto.id_classe, titulo, artista "+
                                    " from produto, produto_familia " +
                                    " where produto.id_produto = produto_familia.id_produto " +
                                    " and produto_familia.id_familia in (select id_familia from produto_familia where id_produto = ?)"+
                                    " and produto.id_produto <> ?";

                            tx.executeSql(sql, [id_produto, id_produto], function (tx, rs) {
                                var relacionados = [];
                                if (rs != null && rs.rows != null) {
                                    for (var i = 0; i < rs.rows.length; i++) {
                                        var row = rs.rows.item(i);
                                        if (row.id_classe == 1){
                                            row.origem = '#panel-home';
                                        } else {
                                            row.origem = '#panel-produtos' + row.id_classe
                                        }
                                        relacionados[relacionados.length] = row;
                                    }
                                }
                                produto['relacionados'] = relacionados;

                                var generos = '';
                                var virgula = '';

                                sql = "select nome from produto_genero, genero where genero.id_genero = produto_genero.id_genero and id_produto = ?";
                                transaction.executeSql(sql, [id_produto], function (tx, rs) {
                                    if (rs != null && rs.rows != null) {
                                        var linhas = [];
                                        for (var i = 0; i < rs.rows.length; i++) {
                                            var row = rs.rows.item(i);
                                            generos = generos + virgula + row.nome;
                                            virgula = ', ';
                                        }
                                        if (generos == '') {
                                            generos = '(nenhum)';
                                        }
                                        produto['generos'] = generos;
                                    }
                                    $('#panel-produtodetalhe').html(app.lancamento(produto));
                                    $('#panel-produtodetalhe').page().enhanceWithin();
                                    $("#popupAdicionaProduto").on("popupafterclose", function () {
                                        controllerProduto.notificaResultadoInclusaoItem();
                                    });
                                    $(":mobile-pagecontainer").pagecontainer("change", "#panel-produtodetalhe");
                                    app.hideLoading();
                                }, controllerProduto.sqlError);
                            }, controllerProduto.sqlError);
                        }, controllerProduto.sqlError);
                    }, controllerProduto.sqlError);
                }, controllerProduto.sqlError);
            }, controllerProduto.sqlError);
        }, controllerProduto.sqlError, controllerProduto.ignore);
    }

};