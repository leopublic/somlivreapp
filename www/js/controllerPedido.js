var controllerPedido = {
    db: null,
    sql: null,
    cache: null,
    // Metodos
    sqlOk: function () {
        console.log('Comando executado com sucesso!');
    },

    sqlError: function(error) {
        var self = this;
        app.hideLoading();
        alert('Erro sqllite: ' + self.sql + ' ' + error.code + error.message);
        console.log('Erro: '+error.message);
        console.log('SQL: '+self.sql);
    },

    ignore: function(){
    },

    // Abre a conexao com o banco de dados, criando o mesmo caso não exista
    abra: function() {
        this.db = window.sqlitePlugin.openDatabase({name: "appvendas.db"});
    },
    
    novo: function(msg){
        var pedido = [];
        pedido['clientes'] = dbo.clientes;
        $('#abrirpedido-content').html(app.abrirpedido(pedido));
        $('#panel-abrirpedido').page().enhanceWithin();
        $(":mobile-pagecontainer").pagecontainer("change", "#panel-abrirpedido");
        if (msg != '' && typeof msg != 'undefined'){
            $( "#msgPopup" ).html(msg);
            $( "#popupMsg" ).popup( "open" );
        }        
    },

    postNovo: function(id_cliente, fantasia){
        var self = this;
        var id = null;
        var sql;
        this.nome_cliente = fantasia;
        this.db.transaction(function(transaction) {
            sql = "update pedido set atual = 0";
            transaction.executeSql(sql, [], function(tx, rs){
                sql = "select max(coalesce(numero, 0)) numero from pedido ";
                transaction.executeSql(sql, [], function(tx, rs){
                    var row = rs.rows.item(0);
                    var numero = row.numero + 1;
                    sql = "insert into pedido (id_cliente, dt_pedido, atual, numero) values (?, datetime('now','localtime'), 1, ?) ";
                    transaction.executeSql(sql, [id_cliente, numero], function(tx, rs){
                        // Deu tudo certo. Redireciona para home
                        dbo.id_pedido = rs.insertId;
                        sql = "select fantasia, razaosocial from cliente where id_cliente = ?";
                        transaction.executeSql(sql, [id_cliente], function(tx, rs){
                            var row = rs.rows.item(0);
                            dbo.alteraCabecalho(row.razaosocial, id_cliente, '');
                            dbo.atualizaCabecalhosProdutos();
                            $(":mobile-pagecontainer").pagecontainer("change", "#panel-home");
                        },self.sqlError);
                    },self.sqlError);
                },self.sqlError);
            },self.sqlError);
        },self.sqlError,self.ignore);
    },
    
    abrirPedido: function(){
        dbo.db.transaction(function (transaction) {
            sql = "select id_pedido, numero, id_condicao_pagamento, cliente, atual,  strftime('%d/%m/%Y - %H:%M:%S', dt_pedido) dt_pedido from pedido where id_pedido = ?";
            dbo.sql = sql;
            transaction.executeSql(sql, [id_pedido], function (tx, rs) {
                var row = rs.rows.item(0);
                pedido = row;
                atual = row.atual;
                sql = "select produto.*, itempedido.id_pedido, itempedido.id_itempedido, itempedido.qtd from itempedido, produto where produto.id_produto = itempedido.id_produto and itempedido.id_pedido = ?";
                dbo.sql = sql;
                transaction.executeSql(sql, [id_pedido], function (tx, rs) {
                    if (rs != null && rs.rows != null && rs.rows.length > 0) {
                        var linhas = [];
                        var valor_total = 0;
                        for (var i = 0; i < rs.rows.length; i++) {
                            var row = rs.rows.item(i);
                            row.total = row.preco * row.qtd;
                            valor_total = valor_total + row.total;

                            row.preco = dbo.numeroFormatado(row.preco, 2);
                            row.qtd = dbo.numeroFormatado(row.qtd, 0);
                            row.total = dbo.numeroFormatado(row.total, 2);
                            linhas[linhas.length] = row;
                        }
                        pedido['produtos'] = linhas;
                        pedido.valor_total = dbo.numeroFormatado(valor_total, 2);
                    } else {
                        pedido.valor_total = 0;
                    }
                    $('#detalhepedido').html(app.detalhepedido(pedido));
                    if (atual) {
                        $('#panel-detalhepedido').children('#tabPedi').addClass('ui-btn-active');
                    } else {
                        $('#panel-detalhepedido').children('#tabPedi').removeClass('ui-btn-active');
                    }
                    $(":mobile-pagecontainer").pagecontainer("change", "#panel-detalhepedido");
                    if (msg != '' && typeof msg != 'undefined') {
                        $("#msgPopup").html(msg);
                        $("#popupMsg").popup("open");
                    }
                }, dbo.sqlError);
            }, dbo.sqlError);
        }, dbo.sqlError, dbo.ignore);
        
    },
    
    reabrir: function (id_pedido){
        this.db.transaction(function(transaction) {
            sql = "select pedido.id_cliente, fantasia, razaosocial from cliente, pedido where cliente.id_cliente = pedido.id_cliente and pedido.id_pedido= ?";
            transaction.executeSql(sql, [id_pedido], function(tx, rs){
                var row = rs.rows.item(0);
                dbo.alteraCabecalho(row.razaosocial, row.id_cliente, '');
                sql = "update pedido set atual = 0";
                transaction.executeSql(sql, [], function(tx, rs){
                    sql = "update pedido set atual = 1 where id_pedido = ?";
                    transaction.executeSql(sql, [id_pedido], function(tx, rs){
                        dbo.id_pedido = id_pedido;
                        dbo.atualizaCabecalhosProdutos();
                        controllerPedido.listar('Pedido reaberto com sucesso');
                    },self.sqlError);
                },self.sqlError);
            },self.sqlError);
        },self.sqlError,self.ignore);
        
    },
    
    cabecalho: function (id_pedido, msg) {
        var self = this;
        var sql;
        var pedido;
        var atual;
        if (id_pedido == '') {
            alert('Nenhum pedido em aberto para ser exibido');
        } else {
            dbo.db.transaction(function (transaction) {
                sql = "select id_pedido, numero, id_cliente, id_condicao_pagamento, atual,  strftime('%d/%m/%Y - %H:%M:%S', dt_pedido) dt_pedido, dt_envio";
                sql+= " from pedido where id_pedido = ?";
                dbo.sql = sql;
                transaction.executeSql(sql, [id_pedido], function (tx, rs) {
                    var row = rs.rows.item(0);
                    if (!row.dt_envio){
                        row.editavel = true;
                    }
                    pedido = row;
                    

                    atual = row.atual;
                    pedido['itensMenu'] = controllerPedido.itensMenu(id_pedido, 'Cabeçalho');
                    
                    var clientes = dbo.clientes;
                    for (var i = 0; i < clientes.length; i++) {
                        if (clientes[i].id_cliente == row.id_cliente){
                            clientes[i].selected = true;
                            break;
                        }
                    }
                    var condpag = dbo.condpag;
                    for (var i = 0; i < condpag.length; i++) {
                        if (condpag[i].id_condicao_pagamento == row.id_condicao_pagamento){
                            condpag[i].selected = true;
                            break;
                        }
                    }
                    pedido['clientes'] = clientes;
                    pedido['condpag'] = condpag;
                    $('#panel-cabecalhopedido').html(app.cabecalhopedido(pedido));
                    $('#panel-cabecalhopedido').page().enhanceWithin();
                    $(":mobile-pagecontainer").pagecontainer("change", "#panel-cabecalhopedido", {reload: true});
                    if (msg != '' && typeof msg != 'undefined') {
                        $("#msgPedidoCabecalho #msgPopup").html(msg);
                        $("#msgPedidoCabecalho").popup("open");
                    }
                }, self.sqlError);
            }, self.sqlError, self.ignore);
        }
    },
    
    atual: function(){
        this.itens(dbo.id_pedido, '');
    },
    
    itens: function(id_pedido, msg){
        var self = this;
        var sql;
        var pedido;
        var atual;
        if (id_pedido == ''){
            alert('Nenhum pedido em aberto para ser exibido');
        } else {
            this.db.transaction(function(transaction) {
                sql = "select id_pedido, razaosocial, atual, pedido.id_cliente, pedido.id_condicao_pagamento, dt_envio, statusSignus";
                sql+= ", strftime('%d/%m/%Y - %H:%M:%S', dt_pedido) dt_pedido";
                sql+= "  from pedido ";
                sql+= "  join cliente on cliente.id_cliente = pedido.id_cliente";
                sql+= " where id_pedido = ?";
                self.sql = sql;
                transaction.executeSql(sql, [id_pedido], function(tx, rs){
                    var row = rs.rows.item(0);
                    if (!row.dt_envio){
                        console.log('O pedido e editavel');
                        row.editavel = true;
                    }
                    pedido = row;
                    
                    
                    atual = row.atual;
                    sql = "select produto.*, itempedido.id_pedido, itempedido.id_itempedido, itempedido.qtd, itempedido.preco_bruto, itempedido.preco_liquido, itempedido.desconto";
                    sql+= "  from itempedido, produto";
                    sql+= " where produto.id_produto = itempedido.id_produto";
                    sql+= "   and itempedido.id_pedido = ?";
                    self.sql = sql;
                    
                    transaction.executeSql(sql, [id_pedido], function(tx, rs){
                        if (rs != null && rs.rows != null && rs.rows.length > 0) {
                            var linhas = [];
                            var val_total = 0;
                            var qtd_total = 0;
                            
                            for (var i = 0; i < rs.rows.length; i++) {
                                var row = rs.rows.item(i);
                                row.total = parseFloat(row.preco_liquido) * parseInt(row.qtd);
                                row.preco_liquido_fmt = dbo.numeroFormatado(row.preco_liquido, 2);
                                row.preco_bruto_fmt = dbo.numeroFormatado(row.preco_bruto, 2);
                                row.titulo_fmt = row.titulo.replace("'", "\\'");
                                row.titulo_fmt = row.titulo_fmt.replace('"', '');
                                row.artista_fmt = row.artista.replace("'", "\\'");
                                row.artista_fmt = row.artista_fmt.replace('"', '');
                                if ($.isNumeric(row.desconto)){
                                    if (parseInt(row.desconto) == 0){
                                        row.desconto_fmt = '--';
                                    } else {
                                        row.desconto_fmt = dbo.numeroFormatado(row.desconto, 2)+'%';
                                    }
                                } else {
                                    row.desconto_fmt = '--';
                                }
                                val_total = val_total + parseFloat(row.total);
                                qtd_total += parseInt(row.qtd);

                                row.qtd = dbo.numeroFormatado(row.qtd, 0);
                                row.total_fmt = dbo.numeroFormatado(row.total, 2);
                                linhas[linhas.length] = row;
                            }
                            pedido['produtos'] = linhas;
                            pedido.val_total_fmt = dbo.numeroFormatado(val_total, 2);
                            pedido.qtd_total_fmt = dbo.numeroFormatado(qtd_total, 0);
                        } else {
                            pedido.val_total_fmt = 0;
                            pedido.qtd_total_fmt = 0;
                        }
                        pedido['itensMenu'] = controllerPedido.itensMenu(id_pedido, 'Produtos');
                        pedido['msg'] = msg;
                        $('#panel-detalhepedido').html(app.detalhepedido(pedido));
                        $('#panel-detalhepedido').page().enhanceWithin();
                        app.msg = msg;
                        $("#resultadoExclusao").popup({transition: "fade"});
                        $("#resultadoExclusao").on("popupafteropen", function (event, ui) {
                            setTimeout(function () {
                                $("#resultadoExclusao").popup("close");
                            }, 1500);
                        });
                        
                        $(":mobile-pagecontainer").pagecontainer("change", "#panel-detalhepedido", {reload: true});
                        if (msg != '' && typeof msg != 'undefined'){
                            $( "#resultadoExclusao" ).popup( "open" );
                        }        
                    },self.sqlError);
                },self.sqlError);
            },self.sqlError,function(){
            });
        }        
    },
    
    itensMenu: function(id_pedido, ativo){
        
        var menu = {
            'Cabeçalho': {texto:'Cabeçalho', href:'', onclick:"controllerPedido.cabecalho('"+id_pedido+"','');", ativo:'' }
           ,'Produtos' : {texto:'Produtos', href:'', onclick:"controllerPedido.itens('"+id_pedido+"','');", ativo:'' }
        };
        menu[ativo].ativo = '1';
        return menu;
    },

    salvar: function(id_pedido, numero, id_cliente, id_condicao_pagamento, fechar){
        var self = this;
        this.db.transaction(function(transaction) {
            sql = "update pedido set id_cliente= ?, id_condicao_pagamento=?, numero=? where id_pedido=?";
            transaction.executeSql(sql, [id_cliente, id_condicao_pagamento, numero, id_pedido], function(tx, rs){
                if (fechar){
                    sql = "update pedido set atual = 0 where id_pedido=?";
                    transaction.executeSql(sql, [id_pedido], function(tx, rs){
                        dbo.id_pedido = '';
                        dbo.alteraCabecalho('', '', '');
                        dbo.atualizaCabecalhosProdutos();
                        self.cabecalho(id_pedido, 'Pedido atualizado e fechado com sucesso');
                    },self.sqlError);
                } else {
                    self.cabecalho(id_pedido, 'Pedido atualizado com sucesso');                    
                }
            },self.sqlError);
        },self.sqlError,self.ignore);
    },
    
    listar: function(msg){
        var self = this;
        var dados = [];
        this.db.transaction(function(transaction) {
            var sql;
            sql = "select id_pedido, pedido.id_cliente, pedido.id_condicao_pagamento, numero, atual, statusSignus, msgSignus";
            sql+=", strftime('%d/%m/%Y<br/>%H:%M:%S', dt_pedido) dt_pedido";
            sql+=", strftime('%d/%m/%Y<br/>%H:%M:%S', dt_envio) dt_envio";
            sql+="     , razaosocial ";
            sql+="     , descricao ";
            sql+="  from pedido ";
            sql+=" left join cliente on cliente.id_cliente = pedido.id_cliente";
            sql+=" left join condicao_pagamento on condicao_pagamento.id_condicao_pagamento= pedido.id_condicao_pagamento";
            sql+=" order by id_pedido desc";
            transaction.executeSql(sql, [], function(tx, rs){
                var pedidos = [];
                for (var i = 0; i < rs.rows.length; i++) {
                    var row = rs.rows.item(i);
                    if (row.atual == 1){
                        row.atual = 'Sim';
                        row.ehAtual = true;
                    } else {
                        row.atual = 'Não';
                        row.ehAtual = false;
                    }
                    
                    if (row.statusSignus == 1){
                        row.alteravel = false;
                    } else {
                        row.alteravel = true;
                    }
                    
                    pedidos[pedidos.length] = row;
                }
                
                dados['pedidos'] = pedidos;
                dados['msg'] = msg;
                $('#panel-pedidos').html(app.pedidos(dados));
                $('#panel-pedidos').page().enhanceWithin();
                
                $("#msgPedidoListar").popup({transition: "fade"});
                $("#msgPedidoListar").on("popupafteropen", function (event, ui) {
                    setTimeout(function () {
                        $("#msgPedidoListar").popup("close");
                    }, 1500);
                });
                
                $( ":mobile-pagecontainer" ).pagecontainer( "change", "#panel-pedidos" );
                if (msg != '' && typeof msg != 'undefined'){
                    $( "#msgPedidoListar" ).popup( "open" );
                }
            },self.sqlError);
        },self.sqlError,self.ignore);
    },
    
    
    getEnviar: function(msg){
        var dados = [];
        $('#panel-envia-pedidos').html(app.tmplPedidoEnviar(dados));
        $('#panel-envia-pedidos').page().enhanceWithin();
        $(":mobile-pagecontainer").pagecontainer("change", "#panel-envia-pedidos");
        if (msg != '' && typeof msg != 'undefined'){
            $( "#msgPopup" ).html(msg);
            $( "#popupMsg" ).popup( "open" );
        }        
    },

    postEnviar: function(){
        var msg = '';
        if (dbo.vendedor_id == ''){
            msg += "\n- Informe o código do vendedor";
        }
        if (dbo.unidade_negocio_id == ''){
            msg += "\n- Informe a unidade de negócios";
        }
        if (dbo.urlSignus == ''){
            msg += "\n- Informe a url de acesso ao SIGNUS";
        }
        if (msg != ''){
            alert("Antes de enviar os pedidos é necessários corrigir algumas configurações:"+msg);
            dbo.abreConfiguracao();
        } else {
            this.recuperaPendentes();            
        }
    },
    
    logaEnvioPedido: function(msg){
        $('#panel-envia-pedidos #logSincronizacao').prepend(msg+'<br/>');
    },
    
    recuperaPendentes: function(){
        var self = this;
        var sql;
        var pedidos = [];
        self.logaEnvioPedido('Recuperando pedidos pendentes');
        this.db.transaction(function(transaction) {
            sql = "select pedido.id_pedido, pedido.id_cliente, pedido.id_condicao_pagamento, numero";
            sql+= "     , strftime('%Y-%m-%dT%H:%M:%S', pedido.dt_pedido) dt_pedido";
            sql+= "     , produto.prod_cod ";
            sql+= "     , itempedido.id_produto, itempedido.qtd, itempedido.preco_bruto, itempedido.preco_liquido ";
            sql+= "  from itempedido ";
            sql+= "  join produto on produto.id_produto = itempedido.id_produto";
            sql+= "  join pedido on pedido.id_pedido = itempedido.id_pedido";
            sql+= "  join cliente on cliente.id_cliente = pedido.id_cliente";
            sql+= " where pedido.dt_envio is null and id_condicao_pagamento is not null";
            sql+= " order by itempedido.id_pedido";
            dbo.sql = sql;
            transaction.executeSql(sql, [], function(tx, rs){
                if (rs.rows.length > 0){
                    var qtd = rs.rows.length;
                    self.logaEnvioPedido(qtd+' pedidos serão enviados.');
                    var xml = controllerPedido.montaXMLEnvio(rs);                    
                    self.enviaParaSignus(xml);                
                } else {
                    self.logaEnvioPedido('Nenhum pedido pronto para envio identificado.');
                }
                self.logaEnvioPedido('Processo finalizado.');
            },self.sqlError);
        },self.sqlError,self.ignore);
    },
    
    montaXMLEnvio: function(rs){
        var self = this;
        var ret = '';
        var id_pedido_ant = '';
        var pedido;
        var item;
        var id_pedido_vendedor;
        var msg = '';
        this.logaEnvioPedido('Montando envelope de envio...');
        ret += "<ListaPedidos>";
        for (var i = 0; i < rs.rows.length; i++) {
                var row = rs.rows.item(i);
                if (id_pedido_ant == '' || row.id_pedido != id_pedido_ant){

                    if (id_pedido_ant != ''){
                        ret += "</Itens></Pedido>";
                    }
                    id_pedido_vendedor = (dbo.vendedor_id*1000000) + row.numero;
                    ret += "<Pedido>";
                    ret += "<PrePedidoID>"+id_pedido_vendedor+"</PrePedidoID>";
                    ret += "<ClienteID>"+row.id_cliente+"</ClienteID>";
                    ret += "<VendedorID>"+dbo.vendedor_id+"</VendedorID>";
                    ret += "<CondicaoPagamentoID>"+row.id_condicao_pagamento+"</CondicaoPagamentoID>";
                    ret += "<UnidadeNegocioID>"+dbo.unidade_negocio_id+"</UnidadeNegocioID>";
                    ret += "<DataVenda>"+row.dt_pedido+"</DataVenda>";
                    ret += "<NroPedidoCliente></NroPedidoCliente>";
                    ret += "<Itens>";
                    self.logaEnvioPedido('Pedido '+id_pedido_ant+' montado');
                    id_pedido_ant = row.id_pedido;
                }
                ret += "<Item>";
                ret += "<ProdutoID>"+row.prod_cod+"</ProdutoID>";
                ret += "<Quantidade>"+row.qtd+"</Quantidade>";
                ret += "<PrecoBruto>"+row.preco_bruto+"</PrecoBruto>";
                ret += "<PrecoLiquido>"+row.preco_liquido+"</PrecoLiquido>";
                ret += "</Item>";                
        }
        self.logaEnvioPedido('Pedido '+id_pedido_ant+' montado');
        ret += "</Itens></Pedido>";
        ret += "</ListaPedidos>";
        console.log(ret);
        return ret;
    },
    
    enviaParaSignus: function(strXML){
        var self = this;
        self.logaEnvioPedido('Enviando envelope para o SIGNUS');
        var metodo = "RegistrarPedidos";
        var sr= "<?xml version='1.0' encoding='UTF-8'?>";
            sr+="<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://tempuri.org/\">";
            sr+="<SOAP-ENV:Body>";
            sr+="<ns1:"+metodo+">";
            sr+="<ns1:strXML><![CDATA[<?xml version=\"1.0\"?>"+strXML+"]]><\/ns1:strXML>";
            sr+="<\/ns1:"+metodo+">";
            sr+="<\/SOAP-ENV:Body>";
            sr+="<\/SOAP-ENV:Envelope>";

        var msg = '';
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST",dbo.urlSignus,true);
        // Send the POST request
        xmlhttp.setRequestHeader("Accept", "text/html", "text/xml", "\*/\*");
        xmlhttp.setRequestHeader("SOAPAction", "http://tempuri.org/IWSIntegracaoCMS/"+metodo);
        xmlhttp.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
        xmlhttp.send(sr);
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    var response = xmlhttp.responseText;
                    console.log(response);
                    self.processaRetornoEnvio(response);
               }
            }
        };
    },
    
    processaRetornoEnvio: function(response){
        var self = this;
        var sql;
        var retorno = $(response).find('RegistrarPedidosResult').text();
        retorno = retorno.replace(' encoding="null"', '');
        var retornoXML = $.parseXML(retorno);
        var id_pedido;
        var pre_pedido;
        var statusSignus;
        var msgSignus;
        var msg;
        var atual ;
        var dt_envio;
        this.db.transaction(function(tx){
            $(retornoXML).find('ListaPedidos').each(function(){
                $(this).find('Pedido').each(function(){
                    pre_pedido = $(this).find('PrePedidoID').text();
                    statusSignus = $(this).find('Status').text();
                    msgSignus = $(this).find('MensagemErro').text().replace(/(?:\r\n|\r|\n)/g, '<br />');
                    if (statusSignus == '1'){
                        msgSignus = "Pedido enviado com sucesso";
                        atual = " , atual = 0";
                        dt_envio = "      , dt_envio= datetime('now','localtime')";
                    } else {
                        dt_envio = "";
                        atual = '';
                    }
                    id_pedido = parseInt(pre_pedido) - ((parseInt(dbo.vendedor_id) *1000000));
                    msg = 'Resultado pedido '+ id_pedido + ': ('+ statusSignus + ') ' + msgSignus;
                    controllerPedido.logaEnvioPedido(msg);
                    sql="  update pedido ";
                    sql+="    set statusSignus =?";
                    sql+="      , msgSignus=?";
                    sql+=atual;
                    sql+=dt_envio;
                    sql+=" where id_pedido = ?";
                    tx.executeSql(sql, [statusSignus, msgSignus, id_pedido], self.ignore, self.sqlError);
                });
            });
        });            
    },
    
    excluir: function (id_pedido) {
        var self = this;
        var sql;
        this.db.transaction(function (transaction) {
            sql = "delete from itempedido where id_pedido = ?";
            self.sql = sql;
            transaction.executeSql(sql, [id_pedido], function (tx, rs) {
                sql = "delete from pedido where id_pedido = ?";
                dbo.sql = sql;
                transaction.executeSql(sql, [id_pedido], function (tx, rs) {
                    self.listar('Pedido excluído com sucesso.');
                }, self.sqlError);
            }, self.sqlError);
        }, self.sqlError, self.ignore);
    },
    
    excluirItem: function (id_itempedido, id_pedido) {
        var self = this;
        var sql;
        this.db.transaction(function (transaction) {
            sql = "delete from itempedido where id_itempedido = ?";
            self.sql = sql;
            transaction.executeSql(sql, [id_itempedido]
                ,  function(){
                    controllerPedido.itens(id_pedido, 'Produto excluído com sucesso');
                }  
                , self.sqlError);
        }, function(){
        });
    },
    
    confirmaExclusaoItem: function (id_itempedido, catalogo, artista, titulo, id_pedido) {
        var self = this;
        var msg = 'O produto ' + catalogo + ' - "' + titulo + '" (' + artista + ') será retirado do pedido.<br>Essa ação não poderá ser desfeita.';
        $('#descricaoItem').html(msg);
        $('#acaoConfirmaItemOk').off('click');
        $('#acaoConfirmaItemOk').on('click', function () {
            controllerPedido.excluirItem(id_itempedido, id_pedido);
        });
        $("#confirmaExclusaoItem").popup("open");
    },
    
    confirmaExclusaoPedido: function (id_pedido, cliente) {
        var self = this;
        var msg = 'O pedido para o cliente ' + cliente + ' será excluído e todos os itens adicionados serão perdidos. <br/><br/>Essa operação não pode ser desfeita.';
        $('#descricaoConfirmaPedido').html(msg);
        $('#acaoConfirmaPedidoOk').off('click');
        $('#acaoConfirmaPedidoOk').on('click', function () {
            controllerPedido.excluir(id_pedido);
        });
        $("#confirmaExclusaoPedido").popup("open");
    },

    adicionarItem: function (id_produto, qtd, preco, desconto) {
        //
        // Consiste se o pedido está ok
        var msg = '';
        var br = '';
        var id_pedido = dbo.id_pedido;
        if (dbo.id_pedido == '') {
            msg+=br+'Primeiro abra um pedido';
            br = '<br/>';
        }
        if (qtd == '' || typeof qtd == 'undefined'){
            msg+=br+"Informe a quantidade";
            br = '<br/>';
        }
        if (preco == '' || typeof preco == 'undefined'){
            msg+=br+"Informe o preço";
            br = '<br/>';
        }
        if (msg != ''){
            $('#frmAdicionaProduto #msg').html(msg);
            return false;
        }
        //
        // Prepara input
        var self = this;
        var qtdAntiga = null;
        preco = preco.replace(".", "").replace(",", ".");
        if( desconto != ''){
            if (typeof desconto == 'undefined'){
                desconto = 0;
            } else {
                desconto = desconto.replace(".", "").replace(",", ".");                
            }
        }
        var preco_liquido = preco;
        if (parseInt(desconto) > 0){
            preco_liquido = parseFloat(preco) * (1 - (parseFloat(desconto)/100));
        }
        
        //
        // Atualiza banco de dados        
        this.db.transaction(function (transaction) {
            var sql;
            sql = "select * from itempedido where id_pedido = ? and id_produto = ?";
            transaction.executeSql(sql, [id_pedido, id_produto], function (tx, rs) {
                if (rs != null && rs.rows != null && rs.rows.length > 0) {
                    var row = rs.rows.item(0);
                    qtdAntiga = row.qtd;
                    sql = "update itempedido set qtd = ?, preco_bruto = ?, preco_liquido = ?, desconto = ?  where id_pedido = ? and id_produto = ?";
                    transaction.executeSql(sql, [qtd, preco, preco_liquido, desconto, id_pedido, id_produto], function (tx, rs) {
                        controllerProduto.resultado = "Quantidade do produto alterada de " + qtdAntiga + " para " + qtd + "!";
                        controllerProduto.ret = 1;
                    }, self.sqlError);
                } else {
                    sql = "insert into itempedido(id_pedido, id_produto, qtd, preco_bruto, preco_liquido, desconto) values (?,?,?, ?, ?, ?)";
                    transaction.executeSql(sql, [id_pedido, id_produto, qtd, preco, preco_liquido, desconto], function (tx, rs) {
                        controllerProduto.resultado = "Produto adicionado com sucesso ao pedido ("+id_pedido+")";
                        controllerProduto.ret = 1;
                    }, self.sqlError);
                }
            }, self.sqlError);
        }, self.sqlError, function(){
            $( "#popupAdicionaProduto" ).popup( "close" );
        });
    },

    
};
