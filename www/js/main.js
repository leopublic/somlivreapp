var app = {
    titulo: null,
    msg: null,
    
    showMsg: function(){
        var xmsg ;
        if (app.msg != ''){
            xmsg = app.msg;
            app.msg = '';
            $("#msgPopup").html(msg);
            $("#popupMsg").popup("open");            
        }
    },
    
    setTitulo: function(texto){
        this.titulo = texto;
    },
    getTitulo: function(){
        return this.titulo;
    },
    
    showAlert: function (message, title) {
        if (title == null){
            title = "Appvendas";
        }
        if (navigator.notification) {
            navigator.notification.alert(message, null, title, 'OK');
        } else {
            alert(title ? (title + ": " + message) : message);
        }
    },

    showLoading: function(msgText){
        $.mobile.loading( "show", {
                text: msgText,
                textVisible: true,
                theme: 'b',
                textonly: false
        });
    },

    hideLoading: function(msgText){
        $.mobile.loading("hide");
    },

    findByName: function() {
        console.log('findByName');
        this.store.findByName($('.search-key').val(), function(employees) {
            var l = employees.length;
            var e;
            $('.employee-list').empty();
            for (var i=0; i<l; i++) {
                e = employees[i];
                $('.employee-list').append('<li><a href="#employees/' + e.id + '">' + e.firstName + ' ' + e.lastName + '</a></li>');
            }
        });
    },

    sincroniza: function(){
        //var urlbase = config.url_atualizacao;
        var urlbase = "http://appvendas.somlivre.com/sinc/";
        this.showAlert('Entrou pelo menos...');
        dbo.db.transaction(function(tx) {

            var url = urlbase+'produtos';
            this.logaSincronizacao('Recuperando produtos de '+url+'....</br>');
            $.getJSON( url )
                .done(function( data ) {
                    var l = data.length;
                    this.logaSincronizacao(l+' produtos recuperados<br/>');
                    if (l > 0){
                        var sql = "INSERT OR REPLACE INTO produto " +
                            "(id_produto, catalogo, titulo, artista, preco, imagem_destaque, imagem_listagem, icms_isento, coletanea, mais_vendido, infantil, lancamento, dt_importacao, texto_cheio, texto_reduzido, created_at, updated_at, baixado_em) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, datetime('now'))";
                        var e;
                        for (var i = 0; i < l; i++) {
                            e = data[i];
                            this.logaSincronizacao('Inserindo catálogo '+e.catalogo+' - '+e.titulo+' (id '+e.id_produto+')<br/>');
                            tx.executeSql(sql, [e.id_produto, e.catalogo, e.titulo, e.artista, e.preco, e.imagem_destaque, e.imagem_listagem, e.coletanea, e.mais_vendido, e.infantil, e.lancamento, e.dt_importacao, e.texto_cheio, e.texto_reduzido, e.created_at, e.updated_at], dbo.ignore, dbo.sqlError);
                        }
                    }
                });

            var url = urlbase+'musicas';
            this.logaSincronizacao('Recuperando músicas de '+url+'....</br>');
            $.getJSON( url )
                .done(function( data ) {
                    var l = data.length;
                    this.logaSincronizacao(l+' músicas recuperadas<br/>');
                    if (l > 0){
                        var sql = "INSERT OR REPLACE INTO musica " +
                            "(id_musica, id_produto, nome, autor, artista, duracao, ordem, id_extensao, nome_original, created_at, updated_at, extensao) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        var e;
                        for (var i = 0; i < l; i++) {
                            e = data[i];
                            this.logaSincronizacao('Inserindo música '+e.nome+' - '+e.autor+' (id '+e.id_musica+')<br/>');
                            tx.executeSql(sql, [e.id_musica, e.id_produto, e.nome, e.autor, e.artista, e.duracao, e.ordem, e.id_extensao, e.nome_original, e.created_at, e.updated_at, e.extensao], dbo.ignore, dbo.sqlError);
                        }
                    }
                });

            var url = urlbase+'eventos';
            this.logaSincronizacao('Recuperando eventos de '+url+'....</br>');
            $.getJSON( url )
                .done(function( data ) {
                    var l = data.length;
                    this.logaSincronizacao(l+' eventos recuperadas<br/>');
                    if (l > 0){
                        var sql = "INSERT OR REPLACE INTO evento " +
                            "(id_evento, id_produto, data, titulo, descricao, created_at, updated_at) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        var e;
                        for (var i = 0; i < l; i++) {
                            e = data[i];
                            this.logaSincronizacao('Inserindo evento '+e.data+' - '+e.titulo+' (id '+e.id_evento+')<br/>');
                            tx.executeSql(sql, [e.id_evento, e.id_produto, e.data, e.titulo, e.descricao, e.created_at, e.updated_at, e.extensao], dbo.ignore, dbo.sqlError);
                        }
                    }
                });
         },dbo.sqlErro,dbo.sqlOk);

    },

    msgSincronizaSignusCondpag: function(msg){
        $('#signus-content #logSincronizacao').prepend(msg+'<br/>');
    },

    sincronizaCondpag: function () {
        if (dbo.vendedor_id == '' || dbo.unidade_negocio_id == ''){
            alert('Informe o ID do vendedor e a unidade de negócios na configuração antes de acessar o SIGNUS.');
            return;
        }
        if (dbo.urlSignus == ''){
            alert('Informe a url de acesso ao SIGNUS na configuração antes de acessar o SIGNUS.');
            return;
        }
        $('#signus-content #logSincronizacao').html('');
        var xmlhttp = new XMLHttpRequest();
        var url = dbo.urlSignus;
        xmlhttp.open("POST",url,true);

        var sr= "<?xml version='1.0' encoding='UTF-8'?>";
            sr+="<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://tempuri.org/\">";
            sr+="<SOAP-ENV:Body>";
            sr+="<ns1:ObterCondicoesPagamento>";
            sr+="<ns1:strXML>"+dbo.strXML_consultas()+"<\/ns1:strXML>";
            sr+="<\/ns1:ObterCondicoesPagamento>";
            sr+="<\/SOAP-ENV:Body>";
            sr+="<\/SOAP-ENV:Envelope>";


        // Send the POST request
        xmlhttp.setRequestHeader("Accept", "text/html", "text/xml", "\*/\*");
        xmlhttp.setRequestHeader("SOAPAction", "http://tempuri.org/IWSIntegracaoCMS/ObterCondicoesPagamento");
        xmlhttp.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
        xmlhttp.send(sr);
        // send request
        // ...
        var msg = '';
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    //alert('done use firebug to see response');
                    app.msgSincronizaSignusCondpag('Retorno recebido');
                    var response = xmlhttp.responseText;
                    var retorno = $(response).find('ObterCondicoesPagamentoResult').text();
                    var retornoXML = $.parseXML(retorno);        
                    dbo.db.transaction(function(tx){

                        var sql = "drop table condicao_pagamento;";
                        tx.executeSql(sql, [], dbo.ignore, dbo.sqlError);

                        var sql = "CREATE TABLE condicao_pagamento (" +
                          "id_condicao_pagamento INTEGER NOT NULL PRIMARY KEY," +
                          "descricao VARCHAR(50) NULL)";
                        tx.executeSql(sql, [], dbo.ignore, dbo.sqlError);

                        // Carrega condicoes de pagamento
                        $('#signus-content > #logSincronizacao').prepend('<br/>Iniciando carga');                           
                        var sql = "INSERT INTO condicao_pagamento " +
                            "(id_condicao_pagamento, descricao) " +
                            " VALUES (?, ?)";
                        $(retornoXML).find('CondicaoPagamento').each(function(){
                            app.msgSincronizaSignusCondpag('Atualizando condição de pagamento '+$(this).find('CondicaoPagamentoID').text()+ ': '+$(this).find('Descricao').text()+'<br/>');
                            tx.executeSql(sql, [$(this).find('CondicaoPagamentoID').text(), $(this).find('Descricao').text()], dbo.ignore, dbo.sqlError);                                
                        });
                        dbo.recarregaCondpag();
                        app.msgSincronizaSignusCondpag('Encerrada sincronização de condições de pagamento<br/>');
                    });
                } else {
                    alert('Error '+xmlhttp.status+'\n Response:'+xmlhttp.responseText);
                }
            }
        };
    },

    sincronizaClientes: function(){
        if (dbo.vendedor_id == '' || dbo.unidade_negocio_id == ''){
            alert('Informe o ID do vendedor e a unidade de negócios na configuração antes de acessar o SIGNUS.');
            return;
        }
        if (dbo.urlSignus == ''){
            alert('Informe a url de acesso ao SIGNUS na configuração antes de acessar o SIGNUS.');
            return;
        }
        $('#signus-content #logSincronizacao').html('');
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST",dbo.urlSignus,true);

        var sr= "<?xml version='1.0' encoding='UTF-8'?>";
            sr+="<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://tempuri.org/\">";
            sr+="<SOAP-ENV:Body>";
            sr+="<ns1:ObterClientes>";
            sr+="<ns1:strXML>"+dbo.strXML_consultas()+"<\/ns1:strXML>";
            sr+="<\/ns1:ObterClientes>";
            sr+="<\/SOAP-ENV:Body>";
            sr+="<\/SOAP-ENV:Envelope>";


        // Send the POST request
        xmlhttp.setRequestHeader("Accept", "text/html", "text/xml", "\*/\*");
        xmlhttp.setRequestHeader("SOAPAction", "http://tempuri.org/IWSIntegracaoCMS/ObterClientes");
        xmlhttp.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
        xmlhttp.send(sr);
        // send request
        // ...
        var msg = '';
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    //alert('done use firebug to see response');
                    app.msgSincronizaSignusCondpag('Retorno recebido');
                    var response = xmlhttp.responseText;
                    var retorno = $(response).find('ObterClientesResult').text();
                    var retornoXML = $.parseXML(retorno);        
                    dbo.db.transaction(function(tx){                            
                        var sql = "drop table cliente;";
                        tx.executeSql(sql, [], dbo.ignore, dbo.sqlError);

                        var sql = "CREATE TABLE if not exists cliente (" +
                                      "id_cliente INTEGER NOT NULL PRIMARY KEY ," +
                                      "cnpj VARCHAR(14) NULL," +
                                      "razaosocial VARCHAR(50) NULL," +
                                      "fantasia VARCHAR(20) NULL," +
                                      "endereco VARCHAR(100) NULL," +
                                      "bairro VARCHAR(50) NULL," +
                                      "cep VARCHAR(8) NULL," +
                                      "municipio VARCHAR(100) NULL," +
                                      "estadosigla VARCHAR(02) NULL" +
                                      ")";
                        tx.executeSql(sql, [], dbo.ignore, dbo.sqlError);

                        // Carrega clientes
                        app.msgSincronizaSignusCondpag('Iniciando carga de clientes');
                            var sql = "INSERT INTO cliente " +
                                " (id_cliente, cnpj, razaosocial, fantasia, endereco, bairro, cep, municipio, estadosigla)" +
                                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        $(retornoXML).find('Cliente').each(function(){
                                app.logaSincronizacao('Atualizando cliente '+$(this).find('ClienteID').text()+ ': '+$(this).find('RazaoSocial').text()+'<br/>');
                                tx.executeSql(sql, [$(this).find('ClienteID').text(), $(this).find('CNPJ').text(), $(this).find('RazaoSocial').text(), $(this).find('NomeFantasia').text(), $(this).find('Endereco').text(), $(this).find('Bairro').text(), $(this).find('CEP').text(), $(this).find('Municipio').text(), $(this).find('EstadoSigla').text()], dbo.ignore, dbo.sqlError);
                        });
                        app.logaSincronizacao('Encerrada sincronização de clientes<br/>');
                        dbo.recarregaClientes();
                    });
                } else {
                    alert('Error '+xmlhttp.status+'\n Response:'+xmlhttp.responseText);
                }
            }
        };
    },

    logaSincronizacao: function(msg){
        $('#conteudo-atualizacao #logSincronizacao').prepend(msg);
    },

    abreLancamento: function(id_produto){
        dbo.db.transaction(function(tx) {
            var sql;
            var prod = [];
            // Recupera produto
            sql = "select * from produto where id_produto = ?";
            tx.executeSql(sql, [], function(tx, result){
                prod = result;
            }, this.sqlError);

            sql = "select * from musica where id_produto = ?";
            tx.executeSql(sql, [], function(tx, result){
                prod['musicas'] = result;
            }, this.sqlError);

            sql = "select * from anexo where id_produto = ?";
            tx.executeSql(sql, [], function(tx, result){
                prod['anexos'] = result;
            }, this.sqlError);

            sql = "select * from evento where id_produto = ?";
            tx.executeSql(sql, [], function(tx, result){
                prod['eventos'] = result;
            }, this.sqlError);

        },this.sqlError,this.sqlOk);

    },

    formatNumber: function (numero){
        var parts = (numero + "").split(".");
        var output = parts[0];
        // put decimal part back
        var decimal = '';
        if (parts.length > 1){
            decimal = parts[1]+'00';
        } else {
            decimal = '00';
        }
        output += "," + decimal.substr(0,2);
        return output;
    },

    initialize: function() {
        this.compilaTemplates();
        // Inicializa controllers
        controllerCliente.abra();
        controllerCondicaoPagamento.abra();
        controllerPedido.abra();
        controllerProduto.abra();
        controllerGenero.abra();

        // Carrega tabelas estáticas
        controllerProduto.carregar();
    },
    
    compilaTemplates: function(){
        $.get('templates/cabecalho.mustache.html', function (data) {
            Handlebars.registerPartial('cabecalho',data);
        }, 'html');
  
        app.lancamento = Handlebars.compile($("#lancamento-tpl").html());
        app.coverflow = Handlebars.compile($("#coverflow-tpl").html());
        $.get('templates/catalogoListar.mustache.html', function (data) {
            app.catalogo = Handlebars.compile(data);
        }, 'html');
        $.get('templates/catalogoShow.mustache.html', function (data) {
            app.catalogo_show = Handlebars.compile(data);
        }, 'html');

        app.genero = Handlebars.compile($("#generos-tpl").html());
        $.get('templates/condpagListar.mustache.html', function (data) {
            app.condpag = Handlebars.compile(data);
        }, 'html');

        $.get('templates/pedidoMenu.mustache.html', function (data) {
            Handlebars.registerPartial('pedidoMenu',data);
        }, 'html');
        $.get('templates/pedidoNovo.mustache.html', function (data) {
            app.abrirpedido = Handlebars.compile(data);
        }, 'html');
        $.get('templates/pedidoListar.mustache.html', function (data) {
            app.pedidos = Handlebars.compile(data);
        }, 'html');
        $.get('templates/pedidoDetalhe.mustache.html', function (data) {
            app.detalhepedido = Handlebars.compile(data);
        }, 'html');
        $.get('templates/pedidoCabecalho.mustache.html', function (data) {
            app.cabecalhopedido = Handlebars.compile(data);
        }, 'html');
        $.get('templates/pedidoEnviar.mustache.html', function (data) {
            app.tmplPedidoEnviar = Handlebars.compile(data);
        }, 'html');

        $.get('templates/clientesListar.mustache.html', function (data) {
            app.clientes = Handlebars.compile(data);
        }, 'html');
        $.get('templates/produtoCoverflow.mustache.html', function (data) {
            app.tmplProdutoCoverflow = Handlebars.compile(data);
        }, 'html');
        $.get('templates/produtoListar.mustache.html', function (data) {
            app.tmplProdutoListar = Handlebars.compile(data);
        }, 'html');
        $.get('templates/produtoMenu.mustache.html', function (data) {
            Handlebars.registerPartial('produtoMenu',data);
        }, 'html');
        $.get('templates/produtoListarPorClasse.mustache.html', function (data) {
            app.tmplProdutoListarPorClasse = Handlebars.compile(data);
        }, 'html');
        $.get('templates/generoListar.mustache.html', function (data) {
            app.tmplGeneroListar = Handlebars.compile(data);
        }, 'html');
    }
    
};