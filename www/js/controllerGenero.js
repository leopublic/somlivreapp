var controllerGenero = {
    // Propriedades
    db: null,
    sql: null,
    cache: null,
    // Metodos
    sqlOk: function () {
        console.log('Comando executado com sucesso!');
    },

    sqlError: function(error) {
        var self = this;
        app.hideLoading();
        alert('Erro sqllite: ' + self.sql + ' ' + error.code + error.message);
        console.log('Erro: '+error.message);
    },

    ignore: function(){
    },

    // Abre a conexao com o banco de dados, criando o mesmo caso não exista
    abra: function() {
        this.db = window.sqlitePlugin.openDatabase({name: "appvendas.db"});
    },

    listar: function(msg){
        var self = this;
        this.db.transaction(function(transaction) {
            var sql;
            sql = "select * from genero order by nome";
            transaction.executeSql(sql, [], function(tx, rs){
                var linhas = [];
                for (var i = 0; i < rs.rows.length; i++) {
                    var row = rs.rows.item(i);
                    linhas[linhas.length] = row;
                }
                $('#panel-generos').html(app.tmplGeneroListar(linhas));
                $('#panel-generos').page().enhanceWithin();
                $( ":mobile-pagecontainer" ).pagecontainer( "change", "#panel-generos" );
                if (msg != '' && typeof msg != 'undefined'){
                    $( "#msgPopup" ).html(msg);
                    $( "#popupMsg" ).popup( "open" );
                }
            },self.sqlError);
        },self.sqlError,self.ignore);
    }
    
};
