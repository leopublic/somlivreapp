var controllerPedido = {
    db: null,
    sql: null,
    cache: null,
    
    enviaEnvelope: function(envelope, funcaoOk){
        $('#signus-content #logSincronizacao').html('');
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST","http://189.44.180.59/SIGNUSERPIntegracaoCMSM2/WSIntegracaoCMS.svc",true);

        // Send the POST request
        xmlhttp.setRequestHeader("Accept", "text/html", "text/xml", "\*/\*");
        xmlhttp.setRequestHeader("SOAPAction", "http://tempuri.org/IWSIntegracaoCMS/ObterClientes");
        xmlhttp.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
        xmlhttp.send(sr);
        // send request
        // ...
        var msg = '';
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    //alert('done use firebug to see response');
                    app.msgSincronizaSignusCondpag('Retorno recebido');
                    var response = xmlhttp.responseText;
                    var retorno = $(response).find('ObterClientesResult').text();
                    var retornoXML = $.parseXML(retorno);        
                    dbo.db.transaction(function(tx){                            
                        var sql = "CREATE TABLE if not exists cliente (" +
                                      "id_cliente INTEGER NOT NULL PRIMARY KEY ," +
                                      "cnpj VARCHAR(14) NULL," +
                                      "razaosocial VARCHAR(50) NULL," +
                                      "fantasia VARCHAR(20) NULL," +
                                      "endereco VARCHAR(100) NULL," +
                                      "bairro VARCHAR(50) NULL," +
                                      "cep VARCHAR(8) NULL," +
                                      "municipio VARCHAR(100) NULL," +
                                      "estadosigla VARCHAR(02) NULL" +
                                      ")";
                        tx.executeSql(sql, [], dbo.ignore, dbo.sqlError);

                        var sql = "truncate table cliente;";
                        tx.executeSql(sql, [], dbo.ignore, dbo.sqlError);
                        // Carrega clientes
                        app.msgSincronizaSignusCondpag('Iniciando carga de clientes');
                            var sql = "INSERT OR REPLACE INTO cliente " +
                                " (id_cliente, cnpj, razaosocial, fantasia, endereco, bairro, cep, municipio, estadosigla)" +
                                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        $(retornoXML).find('Cliente').each(function(){
                                app.logaSincronizacao('Atualizando cliente '+$(this).find('ClienteID').text()+ ': '+$(this).find('RazaoSocial').text()+'<br/>');
                                tx.executeSql(sql, [$(this).find('ClienteID').text(), $(this).find('CNPJ').text(), $(this).find('RazaoSocial').text(), $(this).find('NomeFantasia').text(), $(this).find('Endereco').text(), $(this).find('Bairro').text(), $(this).find('CEP').text(), $(this).find('Municipio').text(), $(this).find('EstadoSigla').text()], dbo.ignore, dbo.sqlError);
                        });
                        app.logaSincronizacao('Encerrada sincronização de clientes<br/>');
                    });
                } else {
                    alert('Error '+xmlhttp.status+'\n Response:'+xmlhttp.responseText);
                }
            }
        };
        
    }
}