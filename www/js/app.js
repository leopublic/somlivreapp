// We use an "Immediate Function" to initialize the application to avoid leaving anything behind in the global scope
(function () {

    // /* ---------------------------------- Local Variables ---------------------------------- */
    // var adapter = new MemoryAdapter();
    // adapter.initialize().done(function () {
    //     console.log("Data adapter initialized");
    // });

    /* --------------------------------- Event Registration -------------------------------- */

}());

    function onError(error){
        alert(error.code);
    }

    function baixaArquivo(){
        console.log('Vai tentar baixar');
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotAppRootFileSystem, onError);
    }

    function baixaArquivoCriando(){
        console.log('Vai tentar baixar');
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFileSystemComDir, onError);
    }

    function gotAppRootFileSystem(fileSystem) {
        if (fileSystem == null) {
            console.log("fileSystem nao recebido");
        }
        console.log('filesystem ok!');
        var root = fileSystem.root;
        root.getDirectory("com.m2software.somlivreapp/files",{create:true},gotDir,onError);
    };

    function gotFileSystemComDir(fileSystem) {
        if (fileSystem == null) {
            console.log("fileSystem nao recebido");
        }
        console.log('filesystem ok!');

        var remoteFile = 'http://appvendas.somlivre.com.br/sinc/produtos';
        var localPath  = fileSystem.root.toURL() + 'com.m2software.somlivreapp/files/produtosx.json';

        var ft = new FileTransfer();

        ft.download(remoteFile,localPath, function(entry) {
            console.log("Deu certo!!! Baixou!!!");
          },
          function(error) {
            console.log(error);
            console.log("download error source " + error.source);
            console.log("download error target " + error.target);
            console.log("download error code" + error.code);
          }
        );
    };

    function gotDirWrite(dir){
        DATADIR = dir;
        console.log("Caminho completo do diretorio: "+dir.fullPath);
        dir.getFile('produtos.json', {create: true, exclusive: false}, gotArquivoGrava, onError);

    };

    function gotDir(dir){
        DATADIR = dir;
        console.log("Caminho completo do diretorio: "+dir.fullPath);
        dir.getFile('produtos.json', {create: true, exclusive: false}, gotArquivo, onError);

    };

    function gotFileEntry(fileEntry) {
        fileEntry.file(gotArquivo, fail);
    }

    function gotArquivo(file){
        // get the full path to the newly created file on the device
        var localPath = fileSystem.root.toURL() + file.fullPath;

        // // massage the path for android devices (not tested)
        // if (device.platform === "Android" && localPath.indexOf("file://") === 0) {
        //     localPath = localPath.substring(7);
        // }
        console.log('file.fullPath:'+localPath);
        // download the remote file and save it
        var remoteFile = 'http://appvendas.m2software.com.br/sinc/produtos';
        var fileTransfer = new FileTransfer();
        fileTransfer.download(
            remoteFile
            , localPath
            , function(newFileEntry) {
                console.log('Baixou!');
                // successful download, continue to the next image
            }
            ,function(error) { // error callback for #download
                alert("download error source " + error.source);
                alert("download error target " + error.target);
                alert("upload error code" + error.code);
                alert('Erro no download!'+error.code);
                console.log('Error with #download method.', error);
            }
        );
    }

    function gotArquivoGrava(file){
        // get the full path to the newly created file on the device
        var localPath = file.fullPath;

        // // massage the path for android devices (not tested)
        // if (device.platform === "Android" && localPath.indexOf("file://") === 0) {
        //     localPath = localPath.substring(7);
        // }
        console.log('file.fullPath:'+localPath);
        file.createWriter(
                    function (writer) {
                        var data = 'xxxyyyzzz';
                        writer.write(data);
                        writer.onwrite = function (evt) {  // Called when the request has completed successfully.
                            console.log('Gravou');
                        }
                    },
                    function (err) {
                            console.log('Deu erro:'.err.code);
                    }
                );
    }


    function doTheDl (localPath,fileTransfer) {
        try {
            var uri = encodeURI("http://http://appvendas.m2software.com.br/sinc/produtos");
            window.alert ("Downloading to '" + localPath + "'");
            fileTransfer.download (
                uri,
                localPath,
                function (entry) {
                    try {
                        $("#dbDownloadProgressContainer").text("File saved to " + entry.name + ". Applying script to database...");
                    }
                    catch (e) {
                        window.alert ( e);
                    }

                },
                function (err) {
                    window.alert ("ERROROR!!! - " + err);
                    var errCodeName = err.code;
                    switch (err.code) {
                        case FileTransferError.FILE_NOT_FOUND_ERR:
                            errCodeName ='FILE_NOT_FOUND_ERR';
                            break;
                        case FileTransferError.INVALID_URL_ERR:
                            errCodeName="INVALID_URL_ERR";
                            break;
                        case FileTransferError.CONNECTION_ERR:
                            errCodeName="CONNECTION_ERR";
                            break;
                        case FileTransferError.ABORT_ERR:
                            errCodeName="ABORT_ERR";
                            break;
                        default:
                            errCodeName = "UNKNOWN";
                            break;
                    }
                    window.alert ("Download failed: " + err.source + ", " + err.target + ", " + errCodeName);
                },
                true
            );
        }
        catch (e) {
            window.alert ( e);
        }
    }

    function outroTeste(){
        alert('entrou');
        window.requestFileSystem(
            LocalFileSystem.PERSISTENT
            , 0
            , function(fileSystem) {
                var root = fileSystem.root;
                alert('root:'+fileSystem.root.name) ;
                fileSystem.root.getFile(
                    'produtos.json'
                    , {create: true, exclusive: false}
                    , function(fileEntry) {
                        // get the full path to the newly created file on the device
                        var localPath = fileEntry.fullPath;

                        // // massage the path for android devices (not tested)
                        // if (device.platform === "Android" && localPath.indexOf("file://") === 0) {
                        //     localPath = localPath.substring(7);
                        // }
                        alert('localPath:'+localPath);
                        // download the remote file and save it
                        var remoteFile = encodeURI('http://appvendas.m2software.com.br/sinc/produtos');
                        var fileTransfer = new FileTransfer();
                        fileTransfer.download(
                            remoteFile
                            , localPath
                            , function(newFileEntry) {
                                alert('Baixou!');
                                // successful download, continue to the next image
                            }
                            ,function(error) { // error callback for #download
                                alert("download error source " + error.source);
                                alert("download error target " + error.target);
                                alert("upload error code" + error.code);
                                alert('Erro no download!'+error.code);
                                console.log('Error with #download method.', error);
                            }
                        );
                    }
                    , function(error) { // error callback for #getFile
                        alert('Erro no getFile!'+error.code);
                        console.log('Error with #getFile method.', error);
                    }
                );
            }
            , function(error) { // error callback for #requestFileSystem
                                alert('Erro no requestFileSystem!');
                console.log('Error with #requestFileSystem method.', error);
            }
        );
    }


    function diretorioOk(dir){

    }


    function showLink(url){
        alert(url);
        var divEl = document.getElementById("ready");
        var aElem = document.createElement("a");
        aElem.setAttribute("target", "_blank");
        aElem.setAttribute("href", url);
        aElem.appendChild(document.createTextNode("Ready! Click To Open."))
        divEl.appendChild(aElem);

    }


    function fail(evt) {
        console.log(evt.target.error.code);
    }
