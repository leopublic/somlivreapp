var arquivos = {
    lista: null,
    fs: null,
    item_atual: null,
    status: null,
    localBase: null,
    remoteBase: 'http://appvendas.somlivre.com/media/',
    erroFileSystem: function(error){
        alert(error.code);
    },

    ignore: function(){

    },

    initialize: function(){
        var self = this;
    },

    dirOk: function(fileSystem){
        fileSystem.root.getDirectory("com.m2software.somlivreapp/media/",{create:true},self.ignore,erroFileSystem);
    },

    transferenciaOk: function (entry){
        app.logaSincronizacao('Ok ! ');
        if (item_atual != ''){
            item_atual++;
            this.fluxoBaixaArquivos();
        } else {
            app.logaSincronizacao('Controle de item zerado. Processamento interrompido.<br/>');
        }
    },

    transferenciaErro: function (error){
        var motivo = '';
        if(error.code == FileTransferError.FILE_NOT_FOUND_ERR){
            motivo = 'arquivo não encontrado';
        } else {
            if(error.code == FileTransferError.INVALID_URL_ERR){
                motivo = 'url inválida';
            } else {
                if(error.code == FileTransferError.CONNECTION_ERR){
                    motivo = 'erro de conexão com a internet';
                }
            }
        }
        app.logaSincronizacao('Não foi possível obter o arquivo :'+error.source+' => '+motivo+'<br/>');
        if (this.item_atual != ''){
            this.item_atual++;
            this.fluxoBaixaArquivos();
        } else {
            app.logaSincronizacao('Controle de item zerado. Processamento interrompido.<br/>');
        }
    },

    iniciaBaixaArquivos: function(){
        this.status = 1;
        this.lista = null;
        this.item_atual = null;
        this.fs = null;
        this.fluxoBaixaArquivos();
    },

    fluxoBaixaArquivos: function(){
        var self = this;
        if (this.status == 1){
            if (this.fs == null){
                this.obtemFs();
            } else {
                if (this.lista == null){
                    app.logaSincronizacao('Inicializando lista de produtos para baixar<br/>');
                    this.lista = dbo.dados_sincronizacao.produtos;
                    this.item_atual = 0;
                }
                // Processa lista
                var remoteFile;
                var localFile;
                while (this.item_atual < this.lista.length) {
                    item = this.lista[this.item_atual];
                    remoteFile = this.remoteBase + item.id_produto + '/imagens/capa.jpg';
                    localFile = this.localBase + item.id_produto +'/imagens/capa.jpg';
                    var ft = new FileTransfer();
                    app.logaSincronizacao('Iniciando download da capa do produto '+item.id_produto+'<br/>');
                    ft.download(remoteFile,localFile, function (entry){
                        app.logaSincronizacao('Ok ! ');
                        if (self.item_atual != ''){
                            self.item_atual++;
                            self.fluxoBaixaArquivos();
                        } else {
                            app.logaSincronizacao('Controle de item zerado. Processamento interrompido.<br/>');
                        }
                    }, function (error){
                        var motivo = '';
                        if(error.code == FileTransferError.FILE_NOT_FOUND_ERR){
                            motivo = 'arquivo não encontrado';
                        } else {
                            if(error.code == FileTransferError.INVALID_URL_ERR){
                                motivo = 'url inválida';
                            } else {
                                if(error.code == FileTransferError.CONNECTION_ERR){
                                    motivo = 'erro de conexão com a internet';
                                }
                            }
                        }
                        app.logaSincronizacao('Não foi possível obter o arquivo :'+error.source+' => '+motivo+'<br/>');
                        if (self.item_atual != ''){
                            self.item_atual++;
                            self.fluxoBaixaArquivos();
                        } else {
                            app.logaSincronizacao('Controle de item zerado. Processamento interrompido.<br/>');
                        }
                    });
                    return false;
                }
            }
        } else {
            app.logaSincronizacao('Fluxo interrompido<br/>');
        }
    },

    obtemFs: function(dados){
        var self = this;
        app.logaSincronizacao('Obtendo filesystem...<br/>');
        window.requestFileSystem(LocalFileSystem.PERSISTENT,0,self.obtemFsOk, self.obtemFsErro);
    },
    obtemFsOk: function(fileSystem){
        app.logaSincronizacao('Filesystem adquirido.<br/>');
        arquivos.fs = fileSystem;
        arquivos.localBase  = fileSystem.root.toURL() + 'com.m2software.somlivreapp/media/';
        arquivos.fluxoBaixaArquivos();
    },
    obtemFsErro: function(error){
        this.status = -1;
        app.logaSincronizacao('Não foi possível obter o filesystem :'+JSON.stringify(error)+'<br/>');
    },

    fileSystemDisponivel: function(fileSystem){
        var self = this;
        console.log('Entrou no ok');
        app.logaSincronizacao('File system encontrado... <br/>');
        if (fileSystem == null) {
            console.log("fileSystem nao recebido");
        }
        var dados = dbo.dados_sincronizacao;
        var produtos = dados.produtos;
        var musicas = dados.musicas;
        var anexos = dados.anexos;
        var l;
        var e;
        var remoteBase = 'http://appvendas.somlivre.com/media/';
        var remoteFile;
        var localFile;
        var localBase  = fileSystem.root.toURL() + 'com.m2software.somlivreapp/media/';
        // Baixa capas
        l = produtos.length;
        for (var i = 0; i < l; i++) {
            e = produtos[i];
            remoteFile = remoteBase + e.id_produto + '/imagens/capa.jpg';
            localFile = localBase + e.id_produto +'/imagens/capa.jpg';
            var ft = new FileTransfer();
            ft.download(remoteFile,localFile, self.transferenciaOk, self.transferenciaErro);
            remoteFile = remoteBase + e.id_produto + '/imagens/thumb.jpg';
            localFile = localBase + e.id_produto +'/imagens/thumb.jpg';
            var ft = new FileTransfer();
            ft.download(remoteFile,localFile, self.transferenciaOk, self.transferenciaErro);

        }
        // Baixa capas
        l = musicas.length;
        for (var i = 0; i < l; i++) {
            e = musicas[i];
            remoteFile = remoteBase + e.id_produto + '/audio/'+ e.id_musica + '.' + e.extensao;
            localFile = localBase + e.id_produto +'/audio/'+ e.id_musica + '.' + e.extensao;
            var ft = new FileTransfer();
            ft.download(remoteFile,localFile, self.transferenciaOk, self.transferenciaErro);
        }
        // Baixa capas
        l = anexos.length;
        for (var i = 0; i < l; i++) {
            e = anexos[i];
            remoteFile = remoteBase + e.id_produto + '/videos/'+ e.id_anexo + '.' + e.extensao;
            localFile = localBase + e.id_produto +'/videos/'+ e.id_anexo + '.' + e.extensao;
            var ft = new FileTransfer();
            ft.download(remoteFile,localFile, self.transferenciaOk, self.transferenciaErro);
        }
        app.logaSincronizacao('Transferencia de arquivos concluída<br/>');
        dbo.abreLancamentos();
    },

    obtemArquivosProduto: function(){

    }
}
