var controllerSincronizacao = {
    db: null,
    sql: null,
    cache: null,
    // Metodos
    sqlOk: function () {
        console.log('Comando executado com sucesso!');
    },

    sqlError: function(error) {
        var self = this;
        app.hideLoading();
        alert('Erro sqllite: ' + self.sql + ' ' + error.code + error.message);
        console.log('Erro: '+error.message);
        console.log('SQL: '+self.sql);
    },

    ignore: function(){
    },

    carregaProdutos: function(tx, produtos){
        if (produtos.length > 0) {
            var sql = "INSERT OR REPLACE INTO produto " +
                    "(id_produto, catalogo, titulo, artista, preco"+
                    ", imagem_destaque, imagem_listagem, icms_isento, coletanea, mais_vendido"+
                    ", infantil, lancamento, dt_importacao, texto_cheio, texto_reduzido"+
                    ", created_at, updated_at, baixado_em, id_classe, codigo_barras) " +
                    "VALUES "+
                    " (?, ?, ?, ?, ?"+
                    ", ?, ?, ?, ?, ?"+
                    ", ?, ?, ?, ?, ?"+
                    ", ?, ?, ?, ?, datetime('now'))";
            var e;
            for (var i = 0; i < produtos.length; i++) {
                e = produtos[i];
                app.logaSincronizacao('Atualizando catálogo ' + e.catalogo + ' - ' + e.titulo + ' (id ' + e.id_produto + ')<br/>');
                tx.executeSql(sql, [e.id_produto, e.catalogo, e.titulo, e.artista, e.preco, e.imagem_destaque, e.imagem_listagem, e.icms_isento, e.coletanea, e.mais_vendido, e.infantil, e.lancamento, e.dt_importacao, e.texto_cheio, e.texto_reduzido, e.created_at, e.updated_at, e.id_classe, e.codigo_barras], dbo.ignore, dbo.sqlError);
            }
        } else {
            app.logaSincronizacao('Nenhum produto importado<br/>');
        }
        app.logaSincronizacao('Encerrada sincronização de produtos<br/>');
    },
    
    carregaConfiguracao: function(tx, config){
        sql = "update config set unidade_negocio_id = ? , urlSignus = ? ";
        app.logaSincronizacao('Atualizando configurações<br/>');
        tx.executeSql(sql, [config.UnidadeNegocioID, config.url_signus], dbo.ignore, dbo.sqlError);        
    }
}